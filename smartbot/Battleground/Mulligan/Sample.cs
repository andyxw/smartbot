using System.Collections.Generic;
using SmartBot.Plugins.API;
using SmartBotAPI.Battlegrounds;
using SmartBotAPI.Battlegrounds.Actions;

public class TestBGMulligan : BGMulliganProfile
{
    public Card.Cards HandleMulligan(List<Card.Cards> heroes, string selectedProfile)
    {
		//Friendly names can be found in the Cards.Battleground.Heroes class
        // 友好的名字可以在Cards.Battleground.Heroes类中找到
        return heroes[0];
    }
}