using System;
using System.Collections.Generic;
using System.Linq;
using SmartBot.Database;
using SmartBot.Plugins.API;

namespace SmartBot.Mulligan
{
    [Serializable]
    public class DefaultMulliganProfile : MulliganProfile
    {
        List<Card.Cards> CardsToKeep = new List<Card.Cards>();

        private readonly List<Card.Cards> WorthySpells = new List<Card.Cards>
        {

        };

        public List<Card.Cards> HandleMulligan(List<Card.Cards> choices, Card.CClass opponentClass,
            Card.CClass ownClass)
        {
            //德：DRUID 猎：HUNTER 法：MAGE 骑：PALADIN 牧：PRIEST 贼：ROGUE 萨：SHAMAN 术：WARLOCK 战：WARRIOR 瞎：DEMONHUNTER
            bool HasCoin = choices.Count >= 4;
            int DRUID = 0;//德鲁伊
            int HUNTER = 0;//猎人
            int MAGE = 0;//法师
            int PALADIN = 0;//圣骑士
            int PRIEST = 0;//牧师
            int ROGUE = 0;//盗贼
            int SHAMAN = 0;//萨满
            int WARLOCK = 0;//术士
            int WARRIOR = 0;//战士
            int DEMONHUNTER = 0;//恶魔猎手
            int kuaigong = 0;
            int mansu = 0;
            int flag1 = 0;//石雕凿刀 REV_917
            int flag2 = 0;//图腾巨像 REV_838
            int flag3 = 0;//分裂战斧 ULD_413

            foreach (Card.Cards card in choices)
            {
                if (card == Card.Cards.REV_917//石雕凿刀 REV_917
                ) { flag1 += 1; }
                if (card == Card.Cards.REV_838//图腾巨像 REV_838
                ) { flag2 += 1; }
                if (card == Card.Cards.ULD_413//分裂战斧 ULD_413
                ) { flag3 += 1; }
            }
            Bot.Log("对阵职业" + opponentClass);

            if (opponentClass == Card.CClass.PALADIN)
            {
                PALADIN += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.DRUID)
            {
                DRUID += 1;
                mansu += 1;
            }
            if (opponentClass == Card.CClass.HUNTER)
            {
                HUNTER += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.MAGE)
            {
                MAGE += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.PRIEST)
            {
                PRIEST += 1;
                mansu += 1;
            }
            if (opponentClass == Card.CClass.ROGUE)
            {
                ROGUE += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.SHAMAN)
            {
                SHAMAN += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.WARLOCK)
            {
                WARLOCK += 1;
                mansu += 1;
            }
            if (opponentClass == Card.CClass.WARRIOR)
            {
                WARRIOR += 1;
                kuaigong += 1;
            }
            if (opponentClass == Card.CClass.DEMONHUNTER)
            {
                DEMONHUNTER += 1;
                kuaigong += 1;
            }
            foreach (Card.Cards card in choices)
            {
                if (card == Card.Cards.CS2_039 && mansu > 0// 风怒 CS2_039
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.CS2_039))
                    {
                        Keep(card, "风怒");
                    }
                }
                if (card == Card.Cards.REV_838 && flag3 > 0// 图腾巨像 REV_838
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.REV_838))
                    {
                        Keep(card, "图腾巨像");
                    }
                }
                if (card == Card.Cards.ULD_413 && flag2 > 0// 分裂战斧 ULD_413
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.ULD_413))
                    {
                        Keep(card, "分裂战斧");
                    }
                }
                if (card == Card.Cards.TSC_922// 驻锚图腾 TSC_922
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.TSC_922))
                    {
                        Keep(card, "驻锚图腾");
                    }
                }
                if (card == Card.Cards.CFM_696 && kuaigong + PRIEST > 0// 衰变 CFM_696
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.CFM_696))
                    {
                        Keep(card, "衰变");
                    }
                }
                if (card == Card.Cards.ULD_276// 怪盗图腾 EVIL Totem ID：ULD_276 
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.ULD_276))
                    {
                        Keep(card, "怪盗图腾");
                    }
                }
                if (card == Card.Cards.OG_028 && flag1 > 0// 深渊魔物 OG_028
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.OG_028))
                    {
                        Keep(card, "深渊魔物");
                    }
                }
                if (card == Card.Cards.EX1_244// 图腾之力 EX1_244
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.EX1_244))
                    {
                        Keep(card, "图腾之力");
                    }
                }
                if (card == Card.Cards.GIL_530 && kuaigong > 0// 阴燃电鳗 GIL_530 
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.GIL_530))
                    {
                        Keep(card, "阴燃电鳗");
                    }
                }
                if (card == Card.Cards.DMF_704 && flag1 == 0// 笼斗管理员 DMF_704
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.DMF_704))
                    {
                        Keep(card, "笼斗管理员");
                    }
                }
                if (card == Card.Cards.REV_917// 石雕凿刀 REV_917
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.REV_917))
                    {
                        Keep(card, "石雕凿刀");
                    }
                }
                if (card == Card.Cards.AV_259 && PRIEST + ROGUE > 0// 冰霜撕咬 AV_259
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.AV_259))
                    {
                        Keep(card, "冰霜撕咬");
                    }
                }
                if (card == Card.Cards.EX1_565 && mansu > 0//火舌图腾 EX1_565
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.EX1_565))
                    {
                        Keep(card, "火舌图腾");
                    }
                }
                if (card == Card.Cards.TSC_069// 深海融合怪 TSC_069
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.TSC_069))
                    {
                        Keep(card, "深海融合怪");
                    }
                }
                if (card == Card.Cards.AT_052//图腾魔像 AT_052
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.AT_052))
                    {
                        Keep(card, "图腾魔像");
                    }
                }
                if (card == Card.Cards.REV_921// 锻石师 REV_921 
                )
                {
                    if (!CardsToKeep.Contains(Card.Cards.REV_921))
                    {
                        Keep(card, "锻石师");
                    }
                }
            }
            return CardsToKeep;
        }

        private void Keep(Card.Cards id, string log = "")
        {
            CardsToKeep.Add(id);
            if (log != "")
                Bot.Log(log);
        }

    }
}