using System;
using System.Collections.Generic;
using System.Linq;
using SmartBot.Database;
using SmartBot.Plugins.API;
using SmartBotAPI.Plugins.API;
using SmartBotAPI.Battlegrounds;
using SmartBot.Plugins.API.Actions;


/* Explanation on profiles :
 * 
 * 配置文件中定义的所有值都是百分比修饰符，这意味着它将影响基本配置文件的默认值。
 * 
 * 修饰符值可以在[-10000;范围内设置。 10000]（负修饰符有相反的效果）
 * 您可以为非全局修改器指定目标，这些目标特定修改器将添加到卡的全局修改器+修改器之上（无目标）
 * 
 * 应用的总修改器=全局修改器+无目标修改器+目标特定修改器
 * 
 * GlobalDrawModifier --->修改器应用于卡片绘制值
 * GlobalWeaponsAttackModifier --->修改器适用于武器攻击的价值，它越高，人工智能攻击武器的可能性就越小
 * 
 * GlobalCastSpellsModifier --->修改器适用于所有法术，无论它们是什么。修饰符越高，AI玩法术的可能性就越小
 * GlobalCastMinionsModifier --->修改器适用于所有仆从，无论它们是什么。修饰符越高，AI玩仆从的可能性就越小
 * 
 * GlobalAggroModifier --->修改器适用于敌人的健康值，越高越好，人工智能就越激进
 * GlobalDefenseModifier --->修饰符应用于友方的健康值，越高，hp保守的将是AI
 * 
 * CastSpellsModifiers --->你可以为每个法术设置个别修饰符，修饰符越高，AI玩法术的可能性越小
 * CastMinionsModifiers --->你可以为每个小兵设置单独的修饰符，修饰符越高，AI玩仆从的可能性越小
 * CastHeroPowerModifier --->修饰符应用于heropower，修饰符越高，AI玩它的可能性就越小
 * 
 * WeaponsAttackModifiers --->适用于武器攻击的修饰符，修饰符越高，AI攻击它的可能性越小
 * 
 * OnBoardFriendlyMinionsValuesModifiers --->修改器适用于船上友好的奴才。修饰语越高，AI就越保守。
 * OnBoardBoardEnemyMinionsModifiers --->修改器适用于船上的敌人。修饰符越高，AI就越会将其视为优先目标。
 *
 */

namespace SmartBotProfiles
{
    [Serializable]
    public class standardeggPaladin  : Profile
    {
        private int GetSireDenathriusDamageCount(Card c)
                {
                    return GetTag(c, Card.GAME_TAG.TAG_SCRIPT_DATA_NUM_2);
                }

        private int GetTag(Card c, Card.GAME_TAG tag)
        {
            if (c.tags != null && c.tags.ContainsKey(tag))
                return c.tags[tag];
            return -1;
        }

#region 英雄技能
        //幸运币
        private const Card.Cards TheCoin = Card.Cards.GAME_005;
        //战士
        private const Card.Cards ArmorUp = Card.Cards.HERO_01bp;
        //萨满
        private const Card.Cards TotemicCall = Card.Cards.HERO_02bp;
        //盗贼
        private const Card.Cards DaggerMastery = Card.Cards.HERO_03bp;
        //圣骑士
        private const Card.Cards Reinforce = Card.Cards.HERO_04bp;
        //猎人
        private const Card.Cards SteadyShot = Card.Cards.HERO_05bp;
        //德鲁伊
        private const Card.Cards Shapeshift = Card.Cards.HERO_06bp;
        //术士
        private const Card.Cards LifeTap = Card.Cards.HERO_07bp;
        //法师
        private const Card.Cards Fireblast = Card.Cards.HERO_08bp;
        //牧师
        private const Card.Cards LesserHeal = Card.Cards.HERO_09bp;
        #endregion

#region 英雄能力优先级
        private readonly Dictionary<Card.Cards, int> _heroPowersPriorityTable = new Dictionary<Card.Cards, int>
        {
            {SteadyShot, 9},//猎人
            {LifeTap, 8},//术士
            {DaggerMastery, 7},//盗贼
            {Reinforce, 5},//骑士
            {Fireblast, 4},//法师
            {Shapeshift, 3},//德鲁伊
            {LesserHeal, 2},//牧师
            {ArmorUp, 1},//战士
        };
#endregion

#region 直伤卡牌 标准模式
        //直伤法术卡牌（必须是可打脸的伤害） 需要计算法强
        private static readonly Dictionary<Card.Cards, int> _spellDamagesTable = new Dictionary<Card.Cards, int>
        {
            //萨满
            {Card.Cards.CORE_EX1_238, 3},//闪电箭 Lightning Bolt     CORE_EX1_238
            {Card.Cards.DMF_701, 4},//深水炸弹 Dunk Tank     DMF_701
            {Card.Cards.DMF_701t, 4},//深水炸弹 Dunk Tank     DMF_701t
            {Card.Cards.BT_100, 3},//毒蛇神殿传送门 Serpentshrine Portal     BT_100 
            //德鲁伊

            //猎人
            {Card.Cards.BAR_801, 1},//击伤猎物 Wound Prey     BAR_801
            {Card.Cards.CORE_DS1_185, 2},//奥术射击 Arcane Shot     CORE_DS1_185
            {Card.Cards.CORE_BRM_013, 3},//快速射击 Quick Shot     CORE_BRM_013
            {Card.Cards.BT_205, 3},//废铁射击 Scrap Shot     BT_205 
            //法师
            {Card.Cards.BAR_541, 2},//符文宝珠 Runed Orb     BAR_541 
            {Card.Cards.CORE_CS2_029, 6},//火球术 Fireball     CORE_CS2_029
            {Card.Cards.BT_291, 5},//埃匹希斯冲击 Apexis Blast     BT_291 
            //骑士
            {Card.Cards.CORE_CS2_093, 2},//奉献 Consecration     CORE_CS2_093 
            //牧师
            //盗贼
            {Card.Cards.BAR_319, 2},//邪恶挥刺（等级1） Wicked Stab (Rank 1)     BAR_319
            {Card.Cards.BAR_319t, 4},//邪恶挥刺（等级2） Wicked Stab (Rank 2)     BAR_319t
            {Card.Cards.BAR_319t2, 6},//邪恶挥刺（等级3） Wicked Stab (Rank 3)     BAR_319t2 
            {Card.Cards.CORE_CS2_075, 3},//影袭 Sinister Strike     CORE_CS2_075
            {Card.Cards.TSC_086, 4},//剑鱼 TSC_086
            //术士
            {Card.Cards.CORE_CS2_062, 3},//地狱烈焰 Hellfire     CORE_CS2_062
            //战士
            {Card.Cards.DED_006, 6},//重拳先生 DED_006
            //中立
            {Card.Cards.DREAM_02, 5},//伊瑟拉苏醒 Ysera Awakens     DREAM_02
        };
        //直伤随从卡牌（必须可以打脸）
        private static readonly Dictionary<Card.Cards, int> _MinionDamagesTable = new Dictionary<Card.Cards, int>
        {
            //盗贼
            {Card.Cards.BAR_316, 2},//油田伏击者 Oil Rig Ambusher     BAR_316 
            //萨满
            {Card.Cards.CORE_CS2_042, 4},//火元素 Fire Elemental     CORE_CS2_042 
            //德鲁伊
            //术士
            {Card.Cards.CORE_CS2_064, 1},//恐惧地狱火 Dread Infernal     CORE_CS2_064 
            //中立
            {Card.Cards.CORE_CS2_189, 1},//精灵弓箭手 Elven Archer     CORE_CS2_189
            {Card.Cards.CS3_031, 8},//生命的缚誓者阿莱克丝塔萨 Alexstrasza the Life-Binder     CS3_031 
            {Card.Cards.DMF_174t, 4},//马戏团医师 Circus Medic     DMF_174t
            {Card.Cards.DMF_066, 2},//小刀商贩 Knife Vendor     DMF_066 
            {Card.Cards.SCH_199t2, 2},//转校生 Transfer Student     SCH_199t2 
            {Card.Cards.SCH_273, 1},//莱斯·霜语 Ras Frostwhisper     SCH_273
            {Card.Cards.BT_187, 3},//凯恩·日怒 Kayn Sunfury     BT_187
            {Card.Cards.BT_717, 2},//潜地蝎 Burrowing Scorpid     BT_717 
            {Card.Cards.CORE_EX1_249, 2},//迦顿男爵 Baron Geddon     CORE_EX1_249 
            {Card.Cards.DMF_254, 30},//迦顿男爵 Baron Geddon     CORE_EX1_249 
        };
#endregion


#region 攻击模式和自定义 
      public ProfileParameters GetParameters(Board board)
      {

            var p = new ProfileParameters(BaseProfile.Rush) { DiscoverSimulationValueThresholdPercent = -10 }; 
            //Bot.Log("玩家信息: " + rank+"/n"+Legend);
            int a = (board.HeroFriend.CurrentHealth + board.HeroFriend.CurrentArmor) - BoardHelper.GetEnemyHealthAndArmor(board)<-20?0:(board.HeroFriend.CurrentHealth + board.HeroFriend.CurrentArmor) - BoardHelper.GetEnemyHealthAndArmor(board);
            int healthValue=0;
            //攻击模式切换
// 德：DRUID 猎：HUNTER 法：MAGE 骑：PALADIN 牧：PRIEST 贼：ROGUE 萨：SHAMAN 术：WARLOCK 战：WARRIOR 瞎：DEMONHUNTER
            if(board.HeroFriend.CurrentHealth<=13){
                p.GlobalAggroModifier = -5;
                Bot.Log("保命状态");
            }else if(board.EnemyClass == Card.CClass.PALADIN
                    || board.EnemyClass == Card.CClass.WARRIOR
                    || board.EnemyClass == Card.CClass.WARLOCK
                    || board.EnemyClass == Card.CClass.DEMONHUNTER
                    || board.EnemyClass == Card.CClass.DRUID
                    || board.EnemyClass == Card.CClass.ROGUE)
            {
                p.GlobalAggroModifier = (int)(a * 0.625 + 96.5);
                healthValue=(int)(a * 0.625 + 96.5);
                Bot.Log("攻击值"+(a * 0.625 + 96.5));
            }else
            {
                p.GlobalAggroModifier = (int)(a * 0.625 + 113.5);
                healthValue=(int)(a * 0.625 + 113.5);
                Bot.Log("攻击值"+(a * 0.625 + 113.5));
            }	   

       {
 
        
            int myAttack = 0;
            int enemyAttack = 0;
            
            if (board.MinionFriend != null)
            {
                for (int x = 0; x < board.MinionFriend.Count; x++)
                {
                    myAttack += board.MinionFriend[x].CurrentAtk;
                }
            }

            if (board.MinionEnemy != null)
            {
                for (int x = 0; x < board.MinionEnemy.Count; x++)
                {
                    enemyAttack += board.MinionEnemy[x].CurrentAtk;
                }
            }

            if (board.WeaponEnemy != null)
            {
                enemyAttack += board.WeaponEnemy.CurrentAtk;
            }

            if ((int)board.EnemyClass == 2 || (int)board.EnemyClass == 7 || (int)board.EnemyClass == 8)
            {
                enemyAttack += 1;
            }
            else if ((int)board.EnemyClass == 6)
            {
                enemyAttack += 2;
            }   
         //定义场攻  用法 myAttack <= 5 自己场攻大于小于5  enemyAttack  <= 5 对面场攻大于小于5  已计算武器伤害

            int myMinionHealth = 0;
            int enemyMinionHealth = 0;

            if (board.MinionFriend != null)
            {
                for (int x = 0; x < board.MinionFriend.Count; x++)
                {
                    myMinionHealth += board.MinionFriend[x].CurrentHealth;
                }
            }

            if (board.MinionEnemy != null)
            {
                for (int x = 0; x < board.MinionEnemy.Count; x++)
                {
                    enemyMinionHealth += board.MinionEnemy[x].CurrentHealth;
                }
            }
             // 友方随从数量
            int friendCount = board.MinionFriend.Count;
            // 手牌数量
            int HandCount = board.Hand.Count;
            // 虚空行者 CORE_CS2_065 
            int nonImps= board.MinionFriend.Count(x => x.Template.Id == Card.Cards.CORE_CS2_065);
            int numberKids=board.MinionFriend.Count(card => card.Race == Card.CRace.DEMON)-nonImps;
            int drinkersNe=board.MinionFriend.Count(card => card.Race == Card.CRace.DEMON);
            // Bot.Log("恶魔数量:"+drinkersNe);
            int greatDamage=board.FriendGraveyard.Count(card => CardTemplate.LoadFromId(card).Type == Card.CType.MINION);
            // int whetherCanSlay=(greatDamage*2+myAttack)-(board.HeroEnemy.CurrentHealth+enemyMinionHealth);
            // 不稳定的骷髅 Volatile Skeleton ID：REV_845
            int skeletalMage=board.MinionEnemy.Count(x => x.Template.Id == Card.Cards.REV_845);
            if(board.EnemyClass == Card.CClass.MAGE){
            // Bot.Log("不稳定的骷髅数量"+skeletalMage);
            }
            // 深渊诅咒 Abyssal Curse ID：TSC_955t 
            int numberCurses=board.EnemyGraveyard.Count(card => CardTemplate.LoadFromId(card).Id == Card.Cards.TSC_955t);
            // 血量小于等于2的敌方怪
            int bloodVolumeEquals2=board.MinionEnemy.Count(minion => minion.CurrentHealth <= 2); 
            // 血量小于等于4的敌方怪
            int bloodVolumeEquals4=board.MinionEnemy.Count(minion => minion.CurrentHealth <= 4); 
            int bloodVolumeEquals8=board.MinionEnemy.Count(minion => minion.CurrentHealth >= 8); 
            // 术法诅咒的数量 // 拖入深渊 TSC_956 深渊波流 TSC_924
            int spellCurse=board.Hand.Count(x => x.Template.Id == Card.Cards.TSC_956)+board.Hand.Count(x => x.Template.Id == Card.Cards.TSC_956);
            if(spellCurse>0){
            Bot.Log("法术诅咒数量"+spellCurse);
            }
            // 塔姆辛·罗姆 BAR_918
            if(spellCurse>0&&board.HasCardInHand(Card.Cards.BAR_918)){
                spellCurse+=1;
            }
            // 随从诅咒的数量 // 希拉柯丝教徒 TSC_955 扎库尔 TSC_959
            int numberFollowersCurs=board.Hand.Count(x => x.Template.Id == Card.Cards.TSC_955)+board.Hand.Count(x => x.Template.Id == Card.Cards.TSC_959);
            if(numberFollowersCurs>0){
            Bot.Log("随从诅咒的数量"+numberFollowersCurs);
            }
            // 布莱恩·铜须 Brann Bronzebeard ID：CORE_LOE_077
            if(numberFollowersCurs>0&&board.HasCardInHand(Card.Cards.CORE_LOE_077)){
                numberFollowersCurs+=1;
            }
            // 拥有的诅咒牌数量
            int numberCurseCardsOwn=spellCurse+numberFollowersCurs;
            // 下回合可以造成的诅咒伤害
            int truenumberCurses=0;
            if (numberCurseCardsOwn==1&&board.EnemyCardCount<=9){
                truenumberCurses=numberCurses+1;
            }
            if (numberCurseCardsOwn>=2&&board.EnemyCardCount<=8){
                for (int x = 0; x < numberCurseCardsOwn; x++)
                {
                    truenumberCurses+=(numberCurses+x+1);
                }
            }
            if(numberCurseCardsOwn>0){
            Bot.Log("可以造成的诅咒伤害总"+truenumberCurses);
            Bot.Log("当前诅咒伤害"+numberCurses);
            }
            int handenemycount=board.EnemyCardCount;
            int shengyuxueliang=30-board.HeroFriend.CurrentHealth;
            // 我方坟场暗影法术数量
            int numberShadowSpellsInOurCemetery=board.FriendGraveyard.Count(card => CardTemplate.LoadFromId(card).Id == Card.Cards.TSC_956)//拖入深渊 TSC_956
            +board.FriendGraveyard.Count(card => CardTemplate.LoadFromId(card).Id == Card.Cards.TSC_924);//深渊波流 TSC_924 ;
 #endregion
#region 不送的怪
    
          if(board.HasCardOnBoard(Card.Cards.TSC_962t)){//修饰老巨鳍之口 Gigafin's Maw ID：TSC_962t 
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.TSC_962, new Modifier(350)); //修饰老巨鳍 Gigafin ID：TSC_962 
          }
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.REV_242, new Modifier(150)); //慌乱的图书管理员 REV_242
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.AV_137, new Modifier(100)); //深铁穴居人  AV_137
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.REV_021, new Modifier(350)); //逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021 
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.MAW_031, new Modifier(350)); //冥界侍从 Afterlife Attendant ID：MAW_031 
          //   农夫 SW_319
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.SW_319, new Modifier(250));
        //   如果4费,场上小鬼大于3,手里有阴暗的酒保 Shady Bartender ID：SW_086 ,不送小鬼
        if(board.MaxMana ==4
        &&drinkersNe>=3
        &&board.HasCardInHand(Card.Cards.SW_086)){
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.CORE_GIL_191t, new Modifier(150)); //小鬼 Imp ID：CORE_GIL_191t 
          Bot.Log("如果4费,场上小鬼大于3,手里有阴暗的酒保,不送小鬼");
        }
#endregion
 #region 送的怪
          if(board.HasCardOnBoard(Card.Cards.TSC_962)){//修饰老巨鳍 Gigafin ID：TSC_962 
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.TSC_962t, new Modifier(-100)); //修饰老巨鳍之口 Gigafin's Maw ID：TSC_962t 
          }
          p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.REV_373t, new Modifier(-100)); //具象暗影 Shadow Manifestation ID：REV_373t 
#endregion
#region 旅行商人   SW_307 
      //  如果随从为0,降低旅行商人优先值,如果随从大于等于1可以用
      if(board.MinionFriend.Count <=1
      &&board.HasCardInHand(Card.Cards.SW_307)){
        p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_307, new Modifier(999));
        p.TradeModifiers.AddOrUpdate(Card.Cards.SW_307, new Modifier(-99));
        Bot.Log("旅行商人 交易 ");
      }
      if(board.MinionFriend.Count >=2
      &&board.HasCardInHand(Card.Cards.SW_307)){
        p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_307, new Modifier(-15*friendCount));
        Bot.Log("旅行商人:"+-15*friendCount);
      }
#endregion
#region 香料面包师 Spice Bread Baker   SW_056
        if(board.HasCardInHand(Card.Cards.SW_056)
        &&board.HeroFriend.CurrentHealth<=18
        &&board.Hand.Count>=7
              )
          {
				p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_056, new Modifier(-15*shengyuxueliang));
        Bot.Log("香料面包师  条件1"+ (-15*shengyuxueliang));
          }
        if(board.HasCardInHand(Card.Cards.SW_056)
        &&board.HeroFriend.CurrentHealth<=18
        &&board.Hand.Count<=7
              )
          {
				p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_056, new Modifier(-2*shengyuxueliang));
        Bot.Log("香料面包师  条件2"+ (-10*shengyuxueliang));
          }
#endregion
#region 农夫 SW_319 
    // 如果自己场上有活泼的松鼠,送掉它
    if(board.HasCardInHand(Card.Cards.SW_319)
    &&board.MaxMana ==2 )
    {
    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_319, new Modifier(130)); 
    Bot.Log("农夫,130");
    }
#endregion
#region 慌乱的图书管理员 REV_242
    if(board.HasCardInHand(Card.Cards.REV_242)
    &&board.MaxMana ==1
    &&board.HasCardInHand(Card.Cards.CORE_GIL_191) //恶魔法阵 CORE_GIL_191 
    &&board.HasCardInHand(Card.Cards.GAME_005) //
    &&enemyAttack<3
    )
    {
    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_242, new Modifier(-999)); 
    Bot.Log("有恶魔法阵有硬币,优先出慌乱的图书管理员");
    }
#endregion
#region 小鬼首领 Imp Gang Boss ID：CORE_BRM_006 
    if(board.HasCardInHand(Card.Cards.CORE_BRM_006))
    {
    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_319, new Modifier(-99)); 
    Bot.Log("小鬼首领,-99");
    }
#endregion
#region 锈烂蝰蛇 Rustrot Viper ID：SW_072 
    // 如果自己场上有活泼的松鼠,送掉它
    if(board.HasCardInHand(Card.Cards.SW_072))
    {
    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_072, new Modifier(130)); 
    Bot.Log("锈烂蝰蛇,130");
    }
#endregion
 #region 邪恶的厨师 REV_016
        if(board.HasCardInHand(Card.Cards.REV_016)
        &&myAttack>2
        ){
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_016, new Modifier(-99));  
            Bot.Log("邪恶的厨师 -99");
        }
#endregion
#region 符文秘银杖 Runed Mithril Rod  SW_003
        if(board.HasCardInHand(Card.Cards.SW_003) 
        ){
          p.PlayOrderModifiers.AddOrUpdate(Card.Cards.SW_003, new Modifier(999));
          Bot.Log("符文秘银杖 优先出");
        }
        if(board.HasCardInHand(Card.Cards.SW_003)
        &&enemyAttack<=4
        &&board.MinionEnemy.Count <=2
        &&board.EnemyClass != Card.CClass.MAGE 
        ){
          p.CastWeaponsModifiers.AddOrUpdate(Card.Cards.SW_003, new Modifier(-99));
          Bot.Log("符文秘银杖 -99 条件2");
        }else{
          p.CastWeaponsModifiers.AddOrUpdate(Card.Cards.SW_003, new Modifier(-200));
         }
        if(board.HasCardInHand(Card.Cards.SW_003)
        &&board.ManaAvailable >=5
        &&board.WeaponFriend != null 
        && board.WeaponFriend.Template.Id == Card.Cards.SW_003
        ){
          p.CastWeaponsModifiers.AddOrUpdate(Card.Cards.SW_003, new Modifier(150));
          Bot.Log("符文秘银杖 150");
        }
#endregion

#region 献祭 Immolate ID：TID_718  
            if(board.HasCardInHand(Card.Cards.TID_718)
            ){
                p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TID_718, new Modifier(-50*handenemycount)); 
                Bot.Log("献祭"+(-50*handenemycount));
            }
#endregion
#region 私法程序 Dew Process ID：MAW_024 
            if(board.HasCardInHand(Card.Cards.MAW_024)
            ){
                p.CastSpellsModifiers.AddOrUpdate(Card.Cards.MAW_024, new Modifier(-99)); 
                Bot.Log("私法程序"+(-99));
            }
#endregion
#region 纵火指控 Arson Accusation ID：MAW_001 
            if(board.HasCardInHand(Card.Cards.MAW_001)
            ){
                p.CastSpellsModifiers.AddOrUpdate(Card.Cards.MAW_001, new Modifier(130)); 
                Bot.Log("纵火指控"+(130));
            }
#endregion
#region 恶魔来袭 Demonic Assault ID：SW_088 
            if(board.HasCardInHand(Card.Cards.SW_088)
            &&friendCount>=6
            ){
                p.CastSpellsModifiers.AddOrUpdate(Card.Cards.SW_088, new Modifier(150)); 
                Bot.Log("恶魔来袭"+(150));
            }
#endregion
#region 深渊诅咒 Abyssal Curse ID：TSC_955t 
            if(board.HasCardInHand(Card.Cards.TSC_955t)
            ){
                p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_955t, new Modifier(-150)); 
                Bot.Log("深渊诅咒 -150 ");
            }
#endregion
 #region 恐惧巫妖塔姆辛 Dreadlich Tamsin ID：AV_316 
            if(board.HasCardInHand(Card.Cards.AV_316)
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.AV_316, new Modifier(-99)); 
            Bot.Log("恐惧巫妖塔姆辛 -99");
            }    
#endregion
 #region 鲜血掠夺者古尔丹 Bloodreaver Gul'dan ID：CORE_ICC_831
            if(board.HasCardInHand(Card.Cards.CORE_ICC_831)
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.CORE_ICC_831, new Modifier(-99)); 
            Bot.Log("鲜血掠夺者古尔丹 -99");
            }    
#endregion
 #region 鲜血掠夺者古尔丹 Bloodreaver Gul'dan ID：ICC_831 
            if(board.HasCardInHand(Card.Cards.ICC_831)
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.ICC_831, new Modifier(-99)); 
            Bot.Log("鲜血掠夺者古尔丹 -99");
            }    
#endregion
 #region 加拉克苏斯大王 Lord Jaraxxus ID：CORE_EX1_323  
            if(board.HasCardInHand(Card.Cards.CORE_EX1_323)
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.CORE_EX1_323, new Modifier(-99)); 
            Bot.Log("加拉克苏斯大王 -99");
            }    
#endregion
 #region 深渊波流 TSC_924
            // if(board.HasCardInHand(Card.Cards.TSC_924)
            // ){ 
            // p.PlayOrderModifiers.AddOrUpdate(Card.Cards.TSC_924, new Modifier(-999));
            // Bot.Log("深渊波流后出");
            // }    
            if(board.HasCardInHand(Card.Cards.TSC_924)
            &&board.MinionEnemy.Count<=2
            
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_924, new Modifier(350));
            Bot.Log("深渊波流350");
            }  
#endregion
 #region 牺牲魔典 Grimoire of Sacrifice ID：BAR_910 
            if(board.HasCardInHand(Card.Cards.BAR_910)
            ){ 
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.BAR_910, new Modifier(150)); 
            Bot.Log("牺牲魔典 150");
            }
#endregion
 #region 拖入深渊 TSC_956
            // if(board.HasCardInHand(Card.Cards.TSC_956)
            // &&bloodVolumeEquals4>0
            // ){ 
            // p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_956, new Modifier(-99)); 
            // Bot.Log("拖入深渊 -99");
            // }    
#endregion
 #region 纳斯雷兹姆之触 SW_090
            if(board.HasCardInHand(Card.Cards.SW_090)
            ){ 
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.SW_090, new Modifier(-99));
            }    
#endregion
 #region 深渊诅咒 Abyssal Curse ID：TSC_955t 
            // if(truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
            // &&numberCurseCardsOwn>0
            // ){
            //     // 希拉柯丝教徒 TSC_955
       	    // p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_955, new Modifier(-999));
            //     // 扎库尔 TSC_959
       	    // p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_959, new Modifier(-999));
            //     // 拖入深渊 TSC_956
            // p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_956, new Modifier(-999)); 
            //     // 深渊波流 TSC_924
            // p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_924, new Modifier(-999)); 
            // Bot.Log("咒杀!");
            // }   
            if(board.EnemyCardCount==10){
                // 希拉柯丝教徒 TSC_955
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_955, new Modifier(999));
                // 扎库尔 TSC_959
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_959, new Modifier(999));
                // 拖入深渊 TSC_956
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_956, new Modifier(999)); 
                // 深渊波流 TSC_924
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.TSC_924, new Modifier(999)); 
            Bot.Log("满牌不出!");
           
            } 
#endregion
#region 塔姆辛·罗姆 BAR_918
            var tamsingrome = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.BAR_918);//塔姆辛·罗姆 BAR_918
            var touchNasrezm = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.SW_090);//纳斯雷兹姆之触 SW_090
            var dragIntoAbyss = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.TSC_956);//拖入深渊 TSC_956
            var abyssWaveCurrent = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.TSC_956);//深渊波流 TSC_924
            var sacrificeMagicCode = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.BAR_910);//sacrificeMagicCode BAR_910
            // if(tamsingrome!= null
            // &&touchNasrezm!= null
            // &&board.MinionFriend.Count > 1 
            // &&board.ManaAvailable >= 4
            // &&bloodVolumeEquals2>=2
            // ){
            //     p.ComboModifier = new ComboSet(tamsingrome.Id,touchNasrezm.Id); 
            // Bot.Log("血量小于等于2的敌方怪为2,提高解场combes1");
            // }
            // if(tamsingrome!= null
            // &&sacrificeMagicCode!= null
            // &&board.ManaAvailable >= 4
            // &&bloodVolumeEquals2>=4
            // ){
            //     p.ComboModifier = new ComboSet(tamsingrome.Id,sacrificeMagicCode.Id); 
            // Bot.Log("血量小于等于2的敌方怪为2,提高解场combes1");
            // }
            // if(board.HasCardOnBoard(Card.Cards.BAR_918)
            // &&sacrificeMagicCode!= null
            // &&bloodVolumeEquals2>=4
            // ){
            //     p.ComboModifier = new ComboSet(sacrificeMagicCode.Id); 
            // Bot.Log("血量小于等于2的敌方怪为2,提高解场combes1");
            // }
            // if(board.HasCardOnBoard(Card.Cards.BAR_918)
            // &&touchNasrezm!= null
            // &&bloodVolumeEquals2>=2
            // ){
            //     p.ComboModifier = new ComboSet(touchNasrezm.Id); 
            // Bot.Log("血量小于等于2的敌方怪为2,提高解场combes2");
            // }
           if(tamsingrome!= null
            &&dragIntoAbyss!= null
            &&board.ManaAvailable >= 6
            
            &&board.EnemyCardCount<=8
            ){
                p.ComboModifier = new ComboSet(tamsingrome.Id,dragIntoAbyss.Id);  
                Bot.Log("塔姆辛·罗姆+拖入深渊");
            }
            // if(truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
            // &&numberCurseCardsOwn>0
            // &&tamsingrome!= null
            // &&dragIntoAbyss!= null
            // &&board.ManaAvailable >= 6
            // ){
            //     p.ComboModifier = new ComboSet(tamsingrome.Id,dragIntoAbyss.Id);  
            //     Bot.Log("塔姆辛·罗姆+拖入深渊 咒杀了");
            // }    
            if(board.HasCardOnBoard(Card.Cards.BAR_918)
            &&dragIntoAbyss!= null
            &&board.EnemyCardCount<=8
            
            ){
                p.ComboModifier = new ComboSet(dragIntoAbyss.Id);  
                Bot.Log("塔姆辛·罗姆+拖入深渊2");
            }
            // if(truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
            // &&numberCurseCardsOwn>0
            // &&board.HasCardOnBoard(Card.Cards.BAR_918)
            // &&dragIntoAbyss!= null
            // ){
            //     p.ComboModifier = new ComboSet(dragIntoAbyss.Id);  
            //     Bot.Log("塔姆辛·罗姆+拖入深渊 咒杀了");
            // }  
            if(tamsingrome!= null
            &&abyssWaveCurrent!= null
            &&board.ManaAvailable >= 9
            &&bloodVolumeEquals8>0
            ){
                p.ComboModifier = new ComboSet(tamsingrome.Id,abyssWaveCurrent.Id);  
            Bot.Log("塔姆辛·罗姆+深渊波流");
            }
            // if(truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
            // &&numberCurseCardsOwn>0
            // &&tamsingrome!= null
            // &&abyssWaveCurrent!= null
            // &&board.ManaAvailable >= 9
            // ){
            //      p.ComboModifier = new ComboSet(tamsingrome.Id,abyssWaveCurrent.Id);  
            // Bot.Log("塔姆辛·罗姆+深渊波流 咒杀了");
            // }  
            if(board.HasCardOnBoard(Card.Cards.BAR_918)
            &&abyssWaveCurrent!= null
            &&bloodVolumeEquals8>0
            ){
                p.ComboModifier = new ComboSet(abyssWaveCurrent.Id);  
            Bot.Log("塔姆辛·罗姆+深渊波流2");
            }
            // if(truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
            // &&numberCurseCardsOwn>0
            // &&board.HasCardOnBoard(Card.Cards.BAR_918)
            // &&abyssWaveCurrent!= null
            // ){
            //      p.ComboModifier = new ComboSet(tamsingrome.Id,abyssWaveCurrent.Id);  
            // Bot.Log("塔姆辛·罗姆+深渊波流 咒杀了");
            // }  
            if(board.HasCardInHand(Card.Cards.BAR_918)
            ){
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.BAR_918, new Modifier(130)); 
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.BAR_918, new Modifier(999));
            }   
#endregion
 #region 布莱恩·铜须 Brann Bronzebeard ID：CORE_LOE_077 
            //  场上有铜须,不用小鬼
            if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)){
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_EX1_319, new Modifier(999));//烈焰小鬼 CORE_EX1_319
            }
            var bryanCopperBeard = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.CORE_LOE_077);//布莱恩·铜须 Brann Bronzebeard ID：CORE_LOE_077 
            var herakos = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.TSC_955);//希拉柯丝教徒 TSC_955 
            var zakkour = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.TSC_959);//扎库尔 TSC_959
            var kidKingLafam = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_835t);//小鬼大王拉法姆 Imp King Rafaam ID：REV_835t 

                if(bryanCopperBeard!= null
                &&herakos!= null
                &&board.ManaAvailable >= 6
                &&board.EnemyCardCount<=8
                &&myAttack>enemyAttack
                ){
                    p.ComboModifier = new ComboSet(bryanCopperBeard.Id,herakos.Id); 
                Bot.Log("布莱恩·铜须+希拉柯丝教徒");
                }
                if(bryanCopperBeard!= null
                &&herakos!= null
                &&board.ManaAvailable >= 6
                &&board.EnemyCardCount<=8
                &&truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
                &&numberCurseCardsOwn>0
                ){
                    p.ComboModifier = new ComboSet(bryanCopperBeard.Id,herakos.Id); 
                Bot.Log("布莱恩·铜须+希拉柯丝教徒 咒杀了");
                }
                
                if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)
                &&herakos!= null
                &&board.ManaAvailable >= 3
                &&board.EnemyCardCount<=8
                &&myAttack>enemyAttack
                ){
                    p.ComboModifier = new ComboSet(herakos.Id); 
                Bot.Log("布莱恩·铜须+希拉柯丝教徒");
                }

                if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)
                &&herakos!= null
                &&truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
                &&numberCurseCardsOwn>0
                ){
                    p.ComboModifier = new ComboSet(herakos.Id); 
                Bot.Log("布莱恩·铜须+希拉柯丝教徒 咒杀了");
                }

                if(bryanCopperBeard!= null
                &&zakkour!= null
                &&board.ManaAvailable >= 8
                &&board.EnemyCardCount<=8
                &&myAttack>enemyAttack
                ){
                    p.ComboModifier = new ComboSet(bryanCopperBeard.Id,zakkour.Id); 
                Bot.Log("布莱恩·铜须+扎库尔");
                }

                if(bryanCopperBeard!= null
                &&zakkour!= null
                &&truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
                &&numberCurseCardsOwn>0
                ){
                    p.ComboModifier = new ComboSet(bryanCopperBeard.Id,zakkour.Id); 
                Bot.Log("布莱恩·铜须+扎库尔 咒杀了");
                }

                if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)
                &&zakkour!= null
                &&board.ManaAvailable >= 5
                &&board.EnemyCardCount<=8
                &&myAttack>enemyAttack
                ){
                    p.ComboModifier = new ComboSet(zakkour.Id); 
                Bot.Log("布莱恩·铜须+扎库尔");
                }
                if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)
                &&zakkour!= null
                &&truenumberCurses+myAttack>board.HeroEnemy.CurrentHealth
                &&numberCurseCardsOwn>0
                ){
                    p.ComboModifier = new ComboSet(zakkour.Id); 
                Bot.Log("布莱恩·铜须+扎库尔 咒杀了");
                }

                if(bryanCopperBeard!= null
                &&kidKingLafam!= null
                &&board.ManaAvailable >= 9
                ){
                    p.ComboModifier = new ComboSet(bryanCopperBeard.Id,kidKingLafam.Id); 
                Bot.Log("布莱恩·铜须+小鬼大王拉法姆");
                }
                if(board.HasCardOnBoard(Card.Cards.CORE_LOE_077)
                &&kidKingLafam!= null
                &&board.ManaAvailable >= 6
                ){
                    p.ComboModifier = new ComboSet(kidKingLafam.Id); 
                Bot.Log("布莱恩·铜须+小鬼大王拉法姆");
                }
            
            if(board.HasCardInHand(Card.Cards.CORE_LOE_077)
            ){
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(350)); 
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(999));
            }    
#endregion
 #region 扎库尔 TSC_959
            if(board.HasCardInHand(Card.Cards.TSC_959)
            ){
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_959, new Modifier(150)); 
            }    
#endregion

#region 希拉柯丝教徒 TSC_955
            // if(board.HasCardInHand(Card.Cards.TSC_955)
            // ){
       	    // p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_955, new Modifier(-99)); 
            // Bot.Log("希拉柯丝教徒 -99");
            // }
#endregion
 #region 暗脉女勋爵 Lady Darkvein ID：REV_373 
            if(board.HasCardInHand(Card.Cards.REV_373)
            &&(board.MinionFriend.Count >5||numberShadowSpellsInOurCemetery==0)
            ){
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_373, new Modifier(999)); 
            Bot.Log("暗脉女勋爵 999");
            }else{
       	    p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_373, new Modifier(-99)); 
            }
            var lordDarkVein = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_373);//lordDarkVein
            // var dragIntoAbyss = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.TSC_956);//dragIntoAbyss TSC_956
            if(lordDarkVein != null
            &&dragIntoAbyss!=null
            
            &&board.MaxMana>=8
            &&board.MinionFriend.Count <=5
            &&board.MinionEnemy.Count>0)
            {
                p.ComboModifier = new ComboSet(dragIntoAbyss.Id,lordDarkVein.Id);
                Bot.Log("打combe 拖入深渊 暗脉女勋爵");
            } 
#endregion
 #region 玛克扎尔的小鬼 Malchezaar's Imp ID：KAR_089 
            if(board.HasCardInHand(Card.Cards.KAR_089)
            ){
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.KAR_089, new Modifier(999)); 
            Bot.Log("玛克扎尔的小鬼早点出");
            }
#endregion
 #region 过期货物专卖商 ULD_163 
             if(board.HasCardOnBoard(Card.Cards.ULD_163))
            {
            p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.ULD_163, new Modifier(-5)); 
            Bot.Log("过期货物专卖商,送掉 -5");
            }
             if(board.HasCardInHand(Card.Cards.ULD_163))
            {
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.ULD_163, new Modifier(-99));
            Bot.Log("过期货物专卖商 -999 ");
            }
#endregion
 #region 邪恶低语 Wicked Whispers ID：DMF_119 
            if(board.HasCardInHand(Card.Cards.DMF_119)
            &&board.MinionFriend.Count <3
            ){
           p.CastSpellsModifiers.AddOrUpdate(Card.Cards.DMF_119, new Modifier(130));
            Bot.Log("邪恶低语 130");
            }
#endregion
 #region 古尔丹之手 BT_300
            if(board.HasCardInHand(Card.Cards.BT_300)
            ){
           p.CastSpellsModifiers.AddOrUpdate(Card.Cards.BT_300, new Modifier(250));
            Bot.Log("古尔丹之手 250");
            }
#endregion
 #region 灵魂之火 EX1_308
            if(board.HasCardInHand(Card.Cards.EX1_308)
            ){
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.EX1_308, new Modifier(-50));
            Bot.Log("灵魂之火 后出");
            }
#endregion
 #region 莫瑞甘的灵界 BOT_568
            if(board.HasCardInHand(Card.Cards.BOT_568)
            &&board.ManaAvailable >= 4
            ){
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.BOT_568, new Modifier(-99));
            Bot.Log("莫瑞甘的灵界 -99");
            }
#endregion
 #region 剑圣萨穆罗 BAR_078
       if(board.HasCardInHand(Card.Cards.BAR_078)
        &&(enemyAttack<=4
        &&board.HeroFriend.CurrentHealth>=20
        &&board.MinionEnemy.Count !=1
        )
        ){
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.BAR_078, new Modifier(650)); //巴罗夫领主 Lord Barov  ID：SCH_526
            Bot.Log("剑圣萨穆罗 650");
        }
#endregion
 #region 掩息海星 TSC_926 
            if(board.HasCardInHand(Card.Cards.TSC_926)
            ){
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_926, new Modifier(150)); 
            }
            if(board.HasCardInHand(Card.Cards.TSC_926)
            &&skeletalMage>=3
            &&board.EnemyClass == Card.CClass.MAGE
            ){
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_926, new Modifier(-30*skeletalMage)); 
            Bot.Log("掩息海星对面有小鬼"+(-30*skeletalMage));
            }
#endregion
 #region 被背小鬼 Piggyback Imp ID：AV_309 
            if(board.HasCardInHand(Card.Cards.AV_309)
            ){
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.AV_309, new Modifier(-99)); 
            Bot.Log("被背小鬼 -99");
            }
#endregion
#region 阴暗的酒保 Shady Bartender ID：SW_086 
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&drinkersNe <3
            &&board.FriendDeckCount >2
            ){
           p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(550));
            Bot.Log("阴暗的酒保 550");
            }
            // if(board.HasCardInHand(Card.Cards.SW_086)
            // &&board.EnemyClass != Card.CClass.HUNTER 
            // ){
            // p.PlayOrderModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(-100));
            // Bot.Log("阴暗的酒保出牌顺序降低");
            // }
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&drinkersNe >=3
            &&board.ManaAvailable>=4
            ){
            p.TradeModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(999));
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(-50*drinkersNe));
            Bot.Log("小鬼数量大于2,阴暗的酒保不交易优先值:-"+(50*drinkersNe));
            }else{
            p.PlayOrderModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(-100));
            }
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&board.HasCardInHand(Card.Cards.REV_244t)//调皮的小鬼 Mischievous Imp ID：REV_244t 
            &&board.ManaAvailable>=9
            ){
            p.TradeModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(999));
            Bot.Log("大于9费,有调皮的小鬼,降低酒保交易");
            }
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&board.HasCardInHand(Card.Cards.CORE_GIL_191)//恶魔法阵 Fiendish Circle ID：CORE_GIL_191
            &&board.ManaAvailable>=8
            ){
            p.TradeModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(999));
            Bot.Log("大于8费,有恶魔法阵,降低交易");
            }
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&board.HasCardInHand(Card.Cards.BAR_914t)//小鬼集群（等级2） Imp Swarm (Rank 2) ID：BAR_914t 
            &&board.ManaAvailable>=7
            ){
            p.TradeModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(999));
            Bot.Log("大于7费,有小鬼集群（等级2）,降低交易");
            }
            if(board.HasCardInHand(Card.Cards.SW_086)
            &&board.HasCardInHand(Card.Cards.BAR_914t2)//小鬼集群（等级3） Imp Swarm (Rank 3) ID：BAR_914t2 
            &&board.ManaAvailable>=7
            ){
            p.TradeModifiers.AddOrUpdate(Card.Cards.SW_086, new Modifier(999));
            Bot.Log("大于7费,有小鬼集群（等级3）,降低交易");
            }
#endregion
 #region 深铁穴居人  AV_137  
        if(board.HasCardInHand(Card.Cards.AV_137)
        &&board.MinionFriend.Count<6
        )
        {
         p.PlayOrderModifiers.AddOrUpdate(Card.Cards.AV_137, new Modifier(999)); 
          p.CastMinionsModifiers.AddOrUpdate(Card.Cards.AV_137, new Modifier(-999));

          Bot.Log("深铁穴居人 -999");
        } 
        if(board.HasCardOnBoard(Card.Cards.AV_137)
        &&board.MinionFriend.Count<7
        )
        {
         p.OnBoardFriendlyMinionsValuesModifiers.AddOrUpdate(Card.Cards.AV_137, new Modifier(250)); 
          Bot.Log("深铁穴居人 不送");
        } 
#endregion
#region GAME_005
        // 如果一费有恶魔法证则不用
        if(board.MaxMana ==1
        &&board.Hand.Count(card => card.CurrentCost==1)>0
        &&board.HasCardInHand(Card.Cards.CORE_GIL_191)){
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.GAME_005, new Modifier(999));
        }else if(board.HasCardInHand(Card.Cards.CORE_GIL_191)
        &&board.EnemyClass == Card.CClass.MAGE
        ){
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.GAME_005, new Modifier(5));
        }else{
            p.CastSpellsModifiers.AddOrUpdate(Card.Cards.GAME_005, new Modifier(55));
        }
#endregion
#region 调皮的小鬼 Mischievous Imp ID：REV_244 
       if(board.HasCardInHand(Card.Cards.REV_244)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_244, new Modifier(150)); 
        Bot.Log("调皮的小鬼 150");
      }
#endregion
#region 饥饿的愚人 Famished Fool ID：REV_019  
       if(board.HasCardInHand(Card.Cards.REV_019)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_019, new Modifier(350)); 
        Bot.Log("饥饿的愚人 350");
      }
#endregion
#region 被告希尔瓦娜斯 Sylvanas, the Accused ID：MAW_033 
       if(board.HasCardInHand(Card.Cards.MAW_033)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.MAW_033, new Modifier(350)); 
        Bot.Log("被告希尔瓦娜斯 350");
      }
#endregion
#region 被告希尔瓦娜斯 Sylvanas, the Accused ID：MAW_033t  
       if(board.HasCardInHand(Card.Cards.MAW_033t)
       &&enemyAttack<=8){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.MAW_033t, new Modifier(999)); 
      }else{
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.MAW_033t, new Modifier(350)); 
      }
#endregion
#region 饥饿的愚人 Famished Fool ID：REV_019t  
       if(board.HasCardInHand(Card.Cards.REV_019t)
       &&HandCount<=7
       &&board.FriendDeckCount >0
       ){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_019t, new Modifier(-99)); 
        Bot.Log("饥饿的愚人 -99");
      }
#endregion
#region 调皮的小鬼 Mischievous Imp ID：REV_244t 
       if(board.HasCardInHand(Card.Cards.REV_244t)
       &&board.MinionFriend.Count >5){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_244t, new Modifier(350)); 
        Bot.Log("调皮的小鬼+ 350");
      }
#endregion
#region 典狱长 The Jailer ID：MAW_034 
       if(board.HasCardInHand(Card.Cards.MAW_034)
       &&myAttack>=8
       &&board.ManaAvailable>=10){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.MAW_034, new Modifier(-99)); 
        Bot.Log("典狱长 -99");
      }
#endregion
#region 贪食的吞噬者 Insatiable Devourer ID：REV_017  
       if(board.HasCardInHand(Card.Cards.REV_017)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_017, new Modifier(350)); 
        Bot.Log("贪食的吞噬者 350");
      }
#endregion
#region 小鬼大王拉法姆 Imp King Rafaam ID：REV_835 
       if(board.HasCardInHand(Card.Cards.REV_835)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_835, new Modifier(150)); 
        Bot.Log("小鬼大王拉法姆 150");
      }
#endregion
#region 小鬼大王拉法姆 Imp King Rafaam ID：REV_835t 
       if(board.HasCardInHand(Card.Cards.REV_835t)
       &&board.MinionFriend.Count >=4
       ){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_835t, new Modifier(150));
        Bot.Log("小鬼大王拉法姆+ 150");
      }else{
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_835t, new Modifier(130));
      }
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.REV_835t, new Modifier(999));
#endregion
#region 鱼人吸血鬼 Murlocula ID：REV_957 
       if(board.HasCardInHand(Card.Cards.REV_957)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_957, new Modifier(250)); 
        Bot.Log("鱼人吸血鬼 250");
      }
#endregion
#region 鱼人吸血鬼 Murlocula ID：REV_957t 
       if(board.HasCardInHand(Card.Cards.REV_957t)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_957t, new Modifier(-99)); 
        Bot.Log("鱼人吸血鬼+ -99");
      }
#endregion
#region 邪恶图书馆 Vile Library ID：REV_371 
        var VileLibrary = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_371);
        // 
    //    if(board.HasCardInHand(Card.Cards.REV_371)
    //    &&board.MaxMana ==2
    //    &&board.HasCardInHand(Card.Cards.CORE_GIL_191)//恶魔法阵 CORE_GIL_191 
    //    &&!board.HasCardInHand(Card.Cards.GAME_005)
    //    ){
    //     p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(-150));
    //     p.LocationsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(999));
    //     Bot.Log("2费无硬币有法阵下图书馆");
    //   }
        if(VileLibrary!= null
        &&board.MaxMana ==2
        &&board.HasCardInHand(Card.Cards.CORE_GIL_191)//恶魔法阵 CORE_GIL_191 
        &&!board.HasCardInHand(Card.Cards.GAME_005)
            ){
                p.ComboModifier = new ComboSet(VileLibrary.Id); 
                Bot.Log("2费无硬币有法阵下图书馆");
            }
        // if(board.HasCardInHand(Card.Cards.REV_371)
        // &&friendCount<=6
        // ){
        //     p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(-88));
        //     Bot.Log("上地标");
        // }
        //   if(board.HasCardOnBoard(Card.Cards.REV_371)
        // &&numberKids<2
        // ){
        //     p.LocationsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(9999));
        //     Bot.Log("地标不动");
        // }
        // if(VileLibrary!= null
        //     &&board.MinionFriend.Count <=6
        //     &&board.ManaAvailable>=2
        //     ){
        //         p.ComboModifier = new ComboSet(VileLibrary.Id); 
        //         Bot.Log("邪恶图书馆");
        //     }
       
       if(board.HasCardOnBoard(Card.Cards.REV_371)
       ){
        p.LocationsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(-999,Card.Cards.CORE_CS2_065));
        p.LocationsModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(150,Card.Cards.REV_242));//慌乱的图书管理员 REV_242
      }
       if(board.HasCardOnBoard(Card.Cards.REV_371)
       &&board.EnemyClass == Card.CClass.HUNTER
       &&board.SecretEnemyCount >0
       ){
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.REV_371, new Modifier(999));
        Bot.Log("对面是猎且有奥秘提高地标优先级");
      }
    
#endregion
#region 灾祸降临 REV_245
// 如果场上随从大于3,且手牌数+场上随从数小于10,提高抽牌法术优先级 灾祸降临 REV_245
       if(numberKids>=2
       &&HandCount+numberKids<=10
       &&board.HasCardInHand(Card.Cards.REV_245)
       &&board.FriendDeckCount >=numberKids-1
       ){
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.REV_245, new Modifier(-150*numberKids)); 
        Bot.Log("灾祸降临"+(-150*numberKids));
      }else{
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.REV_245, new Modifier(130)); 
      }
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.REV_245, new Modifier(1500));
#endregion
#region 恶魔法阵 CORE_GIL_191 
      
       if(board.HasCardInHand(Card.Cards.CORE_GIL_191)
       &&board.MinionFriend.Count >=6
       ){
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.CORE_GIL_191, new Modifier(999)); 
        Bot.Log("恶魔法阵999");
      }
       if(board.HasCardInHand(Card.Cards.CORE_GIL_191)
       &&board.MinionFriend.Count <=2
       ){
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.CORE_GIL_191, new Modifier(-150)); 
        Bot.Log("恶魔法阵-150");
      }
       if(board.HasCardInHand(Card.Cards.CORE_GIL_191)
       &&board.HasCardInHand(Card.Cards.REV_835t)//小鬼大王拉法姆 Imp King Rafaam ID：REV_835t 
       &&board.MaxMana >5
       ){
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.CORE_GIL_191, new Modifier(-50));
        Bot.Log("大于五费且有小鬼大王,则出牌优先级降低");
      }
       if(board.HasCardInHand(Card.Cards.CORE_GIL_191)//恶魔法阵 CORE_GIL_191 
       &&board.HasCardInHand(Card.Cards.GAME_005)
       &&!board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.AV_137) //深铁穴居人 AV_137
       &&board.MaxMana ==2
       ){
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.CORE_GIL_191, new Modifier(-999)); 
        Bot.Log("2费有硬币上法阵");
      }
#endregion
#region 小鬼集群（等级1） Imp Swarm (Rank 1) ID：BAR_914
       if(board.HasCardInHand(Card.Cards.BAR_914)
       ){
       	p.CastSpellsModifiers.AddOrUpdate(Card.Cards.BAR_914, new Modifier(350)); 
        Bot.Log("小鬼集群（等级1）"+(350));
      }
#endregion
#region 邪恶船运 Wicked Shipment ID：DED_504 
       if(board.HasCardInHand(Card.Cards.DED_504)
       && board.MinionFriend.Count <=2
       ){
        p.CastSpellsModifiers.AddOrUpdate(Card.Cards.DED_504, new Modifier(-150)); 
        Bot.Log("没随从就出邪恶船运");
      }
#endregion
#region 生命分流 Life Tap   HERO_07bp
        // if(board.Hand.Count<=9
        // &&board.FriendDeckCount >0
        // )
        // { 
        // p.CastHeroPowerModifier.AddOrUpdate(Card.Cards.AV_316hp, new Modifier(-30));//恐惧之链 Chains of Dread ID：AV_316hp 
        // p.PlayOrderModifiers.AddOrUpdate(Card.Cards.AV_316hp, new Modifier(1000));
        // // p.CastHeroPowerModifier.AddOrUpdate(Card.Cards.HERO_07bp, new Modifier(-5));//生命分流 Life Tap ID：HERO_07bp
        // // p.PlayOrderModifiers.AddOrUpdate(Card.Cards.AV_316hp, new Modifier(1000));
        // }
        // // if(board.Hand.Count<=3)
        // // { 
        // // p.CastHeroPowerModifier.AddOrUpdate(Card.Cards.HERO_07bp, new Modifier(15));//生命分流 Life Tap ID：HERO_07bp
        // // Bot.Log("抽一口");
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.HERO_07bp, new Modifier(1000));
        p.PlayOrderModifiers.AddOrUpdate(Card.Cards.AV_316hp, new Modifier(1000));
        // // }
#endregion
#region 剑圣奥卡尼 TSC_032
       if(board.HasCardInHand(Card.Cards.TSC_032)){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_032, new Modifier(-150)); 
        Bot.Log("剑圣奥卡尼 -150");
      }
#endregion
#region 老巨鳍 Gigafin ID：TSC_962 
       if(board.HasCardInHand(Card.Cards.TSC_962)
       &&enemyAttack<8
       ){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_962, new Modifier(999)); 
        Bot.Log("老巨鳍 999");
      }else{
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.TSC_962, new Modifier(350)); 
      }
#endregion
#region 逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021 
    if(board.HasCardInHand(Card.Cards.REV_021)
    &&board.MaxMana<8){
        p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_021, new Modifier(999)); 
    }else{
        p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_021, new Modifier(9999)); 
    }
#endregion

#region 十字路口哨所      BAR_075
      // 如果没有随从,降低4费马桶优先级,如果有则提高优先级
      if(board.HasCardInHand(Card.Cards.BAR_075)
      ){
      p.CastMinionsModifiers.AddOrUpdate(Card.Cards.BAR_075, new Modifier(-100*(friendCount)));//十字路口哨所      BAR_075 
      Bot.Log("十字路口哨所"+-100*friendCount);
      }
      if(board.HasCardInHand(Card.Cards.BAR_075)
      &&(board.EnemyClass == Card.CClass.MAGE
      ||board.EnemyClass == Card.CClass.ROGUE
      )
      ){
      p.CastMinionsModifiers.AddOrUpdate(Card.Cards.BAR_075, new Modifier(-150));//十字路口哨所      BAR_075 
      Bot.Log("十字路口哨所"+-150);
      }
#endregion

#region 大帝伤害 德纳修斯大帝 Sire Denathrius ID：REV_906t 
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906t, new Modifier(9999)); 
            p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906, new Modifier(9999)); 
                
        var SireDenathriusCard = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_906t);
        var flameImpId1 = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.CORE_LOE_077);//铜须
        var flameImpId2 = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_021);//逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021
        var flameImpId3 = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.REV_906t);//德纳修斯大帝 Sire Denathrius ID：REV_906t
            if(SireDenathriusCard != null)
            {
                var sireDenaDamages = GetSireDenathriusDamageCount(SireDenathriusCard);
                Bot.Log("大帝伤害"+sireDenaDamages);
                if((board.HasCardInHand(Card.Cards.CORE_LOE_077))
                &&(board.HasCardInHand(Card.Cards.REV_021))//逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021
                &&board.HasCardInHand(Card.Cards.REV_906t)//德纳修斯大帝 Sire Denathrius ID：REV_906t
                &&board.ManaAvailable >= 9
                &&((sireDenaDamages*2)>=enemyMinionHealth+board.HeroEnemy.CurrentHealth+board.HeroEnemy.CurrentArmor)
                ){
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(-9999)); 
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_021, new Modifier(-9899)); 
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906t, new Modifier(-9799)); 
                p.ComboModifier = new ComboSet(flameImpId1.Id,flameImpId2.Id,flameImpId3.Id);
                Bot.Log("打combe 铜须-逐罪者凯尔萨斯-德纳修斯大帝1");
                }
                if((board.HasCardOnBoard(Card.Cards.CORE_LOE_077))
                &&(board.HasCardOnBoard(Card.Cards.CORE_LOE_077))//逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021
                &&board.HasCardInHand(Card.Cards.REV_906t)//德纳修斯大帝 Sire Denathrius ID：REV_906t
                &&board.ManaAvailable >= 9
                &&((sireDenaDamages*2)>=enemyMinionHealth+board.HeroEnemy.CurrentHealth+board.HeroEnemy.CurrentArmor||enemyAttack>board.HeroFriend.CurrentHealth)
                ){
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906t, new Modifier(-9999)); 
                p.ComboModifier = new ComboSet(flameImpId3.Id);
                Bot.Log("打combe 德纳修斯大帝2");
                }
                if((board.HasCardInHand(Card.Cards.CORE_LOE_077))
                &&(board.HasCardOnBoard(Card.Cards.CORE_LOE_077))//逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021
                &&board.HasCardInHand(Card.Cards.REV_906t)//德纳修斯大帝 Sire Denathrius ID：REV_906t
                &&board.ManaAvailable >= 9
                &&((sireDenaDamages*2)>=enemyMinionHealth+board.HeroEnemy.CurrentHealth+board.HeroEnemy.CurrentArmor||enemyAttack>board.HeroFriend.CurrentHealth)
                ){
               p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(-9999)); 
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906t, new Modifier(-9799));  
                p.ComboModifier = new ComboSet(flameImpId1.Id,flameImpId3.Id);
                Bot.Log("打combe 铜须-德纳修斯大帝3");
                }
                if(board.HasCardInHand(Card.Cards.REV_906t)
                &&(!board.HasCardInHand(Card.Cards.REV_021)//逐罪者凯尔萨斯 Kael'thas Sinstrider ID：REV_021
                ||!board.HasCardInHand(Card.Cards.REV_906t))//德纳修斯大帝 Sire Denathrius ID：REV_906t
                &&((sireDenaDamages)>=enemyMinionHealth+board.HeroEnemy.CurrentHealth+board.HeroEnemy.CurrentArmor-myAttack||enemyAttack>board.HeroFriend.CurrentHealth)
                &&board.ManaAvailable >= 9
                ){ 
                p.CastMinionsModifiers.AddOrUpdate(Card.Cards.REV_906t, new Modifier(-9999)); 
                p.ComboModifier = new ComboSet(flameImpId3.Id);
                Bot.Log("打combe 德纳修斯大帝3");
                }
            }
#endregion
#region 海巨人 CORE_EX1_586
       if(board.HasCardInHand(Card.Cards.CORE_EX1_586)
       &&board.EnemyGraveyard.Contains(Card.Cards.BAR_539)
       &&board.MinionFriend.Count+board.MinionEnemy.Count>0
       ){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_EX1_586, new Modifier(-999)); 
        Bot.Log("超凡后,场上怪>0,海巨人优先级提高");
      }
      if(board.HasCardInHand(Card.Cards.CORE_EX1_586)
       ){
       	p.CastMinionsModifiers.AddOrUpdate(Card.Cards.CORE_EX1_586, new Modifier(-99)); 
        Bot.Log("海巨人优先级提高");
      }
#endregion
#region 枯萎化身塔姆辛 Blightborn Tamsin ID：SW_091t4 
        var Blightborn = board.Hand.FirstOrDefault(x => x.Template.Id == Card.Cards.SW_091t4);
        if(Blightborn != null){
        p.ComboModifier = new ComboSet(Blightborn.Id);
        }
#endregion
#region 攻击优先 卡牌威胁
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.AV_137))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.AV_137, new Modifier(200));
            }//深铁穴居人 AV_137
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_922))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_922, new Modifier(200));
            }//驻锚图腾 Anchored Totem ID：TSC_922 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_515))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_515, new Modifier(200));
            }//豪宅管家俄里翁 Orion, Mansion Manager ID：REV_515 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_959))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_959, new Modifier(200));
            }//扎库尔 TSC_959
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_218))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_218, new Modifier(200));
            }//赛丝诺女士 Lady S'theno ID：TSC_218 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_962))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_962, new Modifier(200));
            }//老巨鳍 Gigafin ID：TSC_962  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_016))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_016, new Modifier(200));
            }//邪恶的厨师 Crooked Cook ID：REV_016 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_828t))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_828t, new Modifier(200));
            }//绑架犯的袋子 Kidnapper's Sack ID：REV_828t 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.KAR_006))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.KAR_006, new Modifier(200));
            }//神秘女猎手 Cloaked Huntress ID：KAR_006 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_332))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_332, new Modifier(200));
            }//心能提取者 Anima Extractor ID：REV_332 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CORE_LOE_077))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(200));
            }//布莱恩·铜须 Brann Bronzebeard ID：CORE_LOE_077  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_011))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_011, new Modifier(200));
            }//嫉妒收割者 The Harvester of Envy ID：REV_011 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_011))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_011, new Modifier(200));
            }//嫉妒收割者 The Harvester of Envy ID：REV_011 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.LOOT_412))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.LOOT_412, new Modifier(200));
            }//狗头人幻术师 Kobold Illusionist ID：LOOT_412 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_950))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_950, new Modifier(200));
            }//海卓拉顿 Hydralodon ID：TSC_950  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_062))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_062, new Modifier(200));
            }//闪金镇豺狼人 Goldshire Gnoll ID：SW_062  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.REV_513))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.REV_513, new Modifier(200));
            }//健谈的调酒师 Chatty Bartender ID：REV_513 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_033))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_033, new Modifier(200));
            }//勘探者车队 Prospector's Caravan ID：BAR_033 （通用）
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.ONY_007))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.ONY_007, new Modifier(200));
            }//监护者哈尔琳 Haleh, Matron Protectorate ID：ONY_007 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CS3_032))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_032, new Modifier(200));
            }//龙巢之母奥妮克希亚 Onyxia the Broodmother ID：CS3_032   
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_431))
             {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_431, new Modifier(200));
            }//花园猎豹 Park Panther ID：SW_431 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.AV_340))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.AV_340, new Modifier(200));
            }//亮铜之翼 Brasswing ID：AV_340 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_458t))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_458t, new Modifier(200));
            }//塔维什的山羊 Tavish's Ram ID：SW_458t 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.WC_006))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.WC_006, new Modifier(200));
            }//安娜科德拉 Lady Anacondra ID：WC_006 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.ONY_004))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.ONY_004, new Modifier(200));
            }//团本首领奥妮克希亚 Raid Boss Onyxia ID：ONY_004 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_032))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_032, new Modifier(200));
            }//剑圣奥卡尼 Blademaster Okani ID：TSC_032 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_319))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_319, new Modifier(200));
            }//农夫 SW_319
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_002))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_002, new Modifier(200));
            }//刺豚拳手 Pufferfist ID：TSC_002
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_218))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_218, new Modifier(200));
            }//赛丝诺女士 Lady S'theno ID：TSC_218 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CORE_LOE_077))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CORE_LOE_077, new Modifier(200));
            }//布莱恩·铜须 Brann Bronzebeard ID：CORE_LOE_077 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_620))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_620, new Modifier(200));
            }//恶鞭海妖 Spitelash Siren ID：TSC_620  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_073))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_073, new Modifier(200));
            }//拉伊·纳兹亚 Raj Naz'jan ID：TSC_073 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DED_006))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DED_006, new Modifier(200));
            }//重拳先生  DED_006 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CORE_AT_029))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CORE_AT_029, new Modifier(200));
            }//锈水海盗 CORE_AT_029  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_074))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_074, new Modifier(200));
            }//前沿哨所 Far Watch Post ID：BAR_074  
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.AV_118))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.AV_118, new Modifier(200));
            }//历战先锋 Battleworn Vanguard ID：AV_118 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.GVG_040))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.GVG_040, new Modifier(200));
            }//沙鳞灵魂行者 Siltfin Spiritwalker ID：GVG_040
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BT_304))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BT_304, new Modifier(200));
            }//改进型恐惧魔王 Enhanced Dreadlord ID：BT_304
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_068))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_068, new Modifier(200));
            }//莫尔葛熔魔 Mo'arg Forgefiend ID：SW_068 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_860))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_860, new Modifier(200));
            }//火焰术士弗洛格尔 Firemancer Flurgl ID：BAR_860
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DED_519))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DED_519, new Modifier(200));
            }//迪菲亚炮手  DED_519
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CFM_807))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CFM_807, new Modifier(200));
            }//大富翁比尔杜 Auctionmaster Beardo ID：CFM_807 
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.TSC_054))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.TSC_054, new Modifier(200));
            }//机械鲨鱼 TSC_054
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.GIL_646))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.GIL_646, new Modifier(200));
            }//发条机器人 Clockwork Automaton ID：GIL_646 
           
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SW_115 ))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SW_115 , new Modifier(200));
            }//伯尔纳·锤喙 Bolner Hammerbeak ID：SW_115 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_237))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_237, new Modifier(200));
            }//狂欢报幕员 Carnival Barker DMF_237 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_217))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_217, new Modifier(200));
            }//越线的游客 Line Hopper DMF_217 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_120))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_120, new Modifier(200));
            }//纳兹曼尼织血者 Nazmani Bloodweaver DMF_120  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_707))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_707, new Modifier(200));
            }//鱼人魔术师 Magicfin DMF_707 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_709))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_709, new Modifier(200));
            }//巨型图腾埃索尔 Grand Totem Eys'or DMF_709

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_082))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_082, new Modifier(200));
            }//暗月雕像 Darkmoon Statue DMF_082 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_082t))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_082t, new Modifier(200));
            }//暗月雕像 Darkmoon Statue     DMF_082t 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_708))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_708, new Modifier(200));
            }//伊纳拉·碎雷 Inara Stormcrash DMF_708

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_102))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_102, new Modifier(200));
            }//游戏管理员 Game Master DMF_102

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DMF_222))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DMF_222, new Modifier(200));
            }//获救的流民 Redeemed Pariah DMF_222

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.ULD_003))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.ULD_003, new Modifier(200));
            }//了不起的杰弗里斯 Zephrys the Great ULD_003

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.GVG_104))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.GVG_104, new Modifier(200));
            }//大胖 Hobgoblin GVG_104

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.UNG_900))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.UNG_900, new Modifier(250));
            }//如果对面场上有灵魂歌者安布拉，提高攻击优先度


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.ULD_240))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.ULD_240, new Modifier(250));
            }//如果对面场上有对空奥术法师 Arcane Flakmage     ULD_240，提高攻击优先度


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.FP1_022 && minion.IsTaunt == false))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.FP1_022, new Modifier(50));
            }//如果对面场上有空灵，降低攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.FP1_004))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.FP1_004, new Modifier(50));
            }//如果对面场上有疯狂的科学家，降低攻击优先度


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BRM_002))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BRM_002, new Modifier(500));
            }//如果对面场上有火妖，提高攻击优先度


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CFM_020))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CFM_020, new Modifier(0));
            }//如果对面场上有缚链者拉兹 Raza the Chained CFM_020，降低攻击优先度                     


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.EX1_608))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.EX1_608, new Modifier(250));
            }//如果对面场上有巫师学徒 Sorcerer's Apprentice     X1_608，提高攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.VAN_EX1_608))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.VAN_EX1_608, new Modifier(250));
            }//如果对面场上有巫师学徒 Sorcerer's Apprentice     VAN_EX1_608，提高攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BOT_447))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BOT_447, new Modifier(-10));
            }//如果对面场上有晶化师，降低攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SCH_600t3))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SCH_600t3, new Modifier(250));
            }//如果对面场上有加攻击的恶魔伙伴，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.DRG_320))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.DRG_320, new Modifier(0));
            }//如果对面场上有新伊瑟拉，降低攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CS2_237))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS2_237, new Modifier(300));
            }//如果对面场上有饥饿的秃鹫 Starving Buzzard CS2_237，提高攻击优先度

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.VAN_CS2_237))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.VAN_CS2_237, new Modifier(300));
            }//如果对面场上有饥饿的秃鹫 Starving Buzzard VAN_CS2_237，提高攻击优先度





            //核心系列和贫瘠之地

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.YOP_031))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.YOP_031, new Modifier(250));
            }//如果对面场上有螃蟹骑士 Crabrider     YOP_031，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_537))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_537, new Modifier(200));
            }//如果对面场上有钢鬃卫兵  BAR_537，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_033))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_033, new Modifier(210));
            }//如果对面场上有勘探者车队 Prospector's Caravan BAR_033，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_035))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_035, new Modifier(200));
            }//如果对面场上有科卡尔驯犬者 Kolkar Pack Runner BAR_035，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_871))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_871, new Modifier(250));
            }//如果对面场上有士兵车队 Soldier's Caravan BAR_871 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_312))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_312, new Modifier(200));
            }//如果对面场上有占卜者车队 Soothsayer's Caravan BAR_312，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_043))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_043, new Modifier(250));
            }//如果对面场上有鱼人宝宝车队 Tinyfin's Caravan BAR_043 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_860))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_860, new Modifier(250));
            }//如果对面场上有火焰术士弗洛格尔 Firemancer Flurgl BAR_860 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_063))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_063, new Modifier(250));
            }//如果对面场上有甜水鱼人斥候 Lushwater Scout BAR_063，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_074))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_074, new Modifier(200));
            }//如果对面场上有前沿哨所  BAR_074 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_720))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_720, new Modifier(230));
            }//如果对面场上有古夫·符文图腾 Guff Runetotem BAR_720 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_038))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_038, new Modifier(200));
            }//如果对面场上有塔维什·雷矛 Tavish Stormpike BAR_038 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_545))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_545, new Modifier(200));
            }//如果对面场上有奥术发光体 Arcane Luminary BAR_545，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_888))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_888, new Modifier(200));
            }//如果对面场上有霜舌半人马 Rimetongue BAR_888  ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_317))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_317, new Modifier(200));
            }//如果对面场上有原野联络人 Field Contact BAR_317，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_918))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_918, new Modifier(250));
            }//如果对面场上有塔姆辛·罗姆 Tamsin Roame BAR_918，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_076))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_076, new Modifier(200));
            }//如果对面场上有莫尔杉哨所 Mor'shan Watch Post BAR_076  ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_890))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_890, new Modifier(200));
            }//如果对面场上有十字路口大嘴巴 Crossroads Gossiper BAR_890 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_082))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_082, new Modifier(200));
            }//如果对面场上有贫瘠之地诱捕者 Barrens Trapper BAR_082，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_540))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_540, new Modifier(200));
            }//如果对面场上有腐烂的普雷莫尔 Plaguemaw the Rotting BAR_540 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_878))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_878, new Modifier(200));
            }//如果对面场上有战地医师老兵 Veteran Warmedic BAR_878，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_048))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_048, new Modifier(200));
            }//如果对面场上有布鲁坎 Bru'kan BAR_048，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_075))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_075, new Modifier(200));
            }//如果对面场上有十字路口哨所  BAR_075，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_744))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_744, new Modifier(200));
            }//如果对面场上有灵魂医者 Spirit Healer BAR_744 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.FP1_028))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.FP1_028, new Modifier(200));
            }//如果对面场上有送葬者 Undertaker FP1_028 ，提高攻击优先度 
            
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CS3_019))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_019, new Modifier(200));
            }//如果对面场上有考瓦斯·血棘 Kor'vas Bloodthorn     CS3_019 ，提高攻击优先度 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CORE_FP1_031))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CORE_FP1_031, new Modifier(200));
            }//如果对面场上有瑞文戴尔男爵 Baron Rivendare     CORE_FP1_031 ，提高攻击优先度 

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CS3_032))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_032, new Modifier(200));
            }//如果对面场上有龙巢之母奥妮克希亚 Onyxia the Broodmother     CS3_032 ，提高攻击优先度   

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SCH_317))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SCH_317, new Modifier(200));
            }//如果对面场上有团伙核心 Playmaker     SCH_317 ，提高攻击优先度  

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_847))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_847, new Modifier(200));
            }//如果对面场上有洛卡拉 Rokara     BAR_847 ，提高攻击优先度  


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CS3_025))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_025, new Modifier(200));
            }//如果对面场上有伦萨克大王 Overlord Runthak     CS3_025 ，提高攻击优先度  


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.YOP_021))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.YOP_021, new Modifier(200));
            }//如果对面场上有被禁锢的凤凰 Imprisoned Phoenix     YOP_021  ，提高攻击优先度  


        //    if ((board.HeroEnemy.CurrentHealth + board.HeroEnemy.CurrentArmor)>= 20
        //      && board.MinionEnemy.Count(x=>x.IsLifeSteal==true && x.Template.Id==Card.Cards.CS3_031)>=1
        //    )
        //    {
        //        p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_031, new Modifier(200));
        //    }//如果对面场上有生命的缚誓者阿莱克丝塔萨 Alexstrasza the Life-Binder     CS3_031 有吸血属性，提高攻击优先度
        //    else
        //    {
        //        p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_031, new Modifier(0));
        //    }//如果对面场上有生命的缚誓者阿莱克丝塔萨 Alexstrasza the Life-Binder     CS3_031 没吸血属性，降低攻击优先度



            if ((board.HeroEnemy.CurrentHealth + board.HeroEnemy.CurrentArmor)>= 20
                && board.MinionEnemy.Count(x=>x.IsLifeSteal==true && x.Template.Id==Card.Cards.CS3_033)>=1
            )
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_033, new Modifier(200));
            }//如果对面场上有沉睡者伊瑟拉 Ysera the Dreamer     CS3_033 有吸血属性，提高攻击优先度
            else
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_033, new Modifier(0));
            }//如果对面场上有沉睡者伊瑟拉 Ysera the Dreamer     CS3_033 没吸血属性，降低攻击优先度

                                   
            if ((board.HeroEnemy.CurrentHealth + board.HeroEnemy.CurrentArmor)>= 20
              && board.MinionEnemy.Count(x=>x.IsLifeSteal==true && x.Template.Id==Card.Cards.CS3_034)>=1
            )
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_034, new Modifier(200));
            }//如果对面场上有织法者玛里苟斯 Malygos the Spellweaver     CS3_034 有吸血属性，提高攻击优先度
            else
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CS3_034, new Modifier(0));
            }//如果对面场上有织法者玛里苟斯 Malygos the Spellweaver     CS3_034 没吸血属性，降低攻击优先度


            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.CORE_EX1_110))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.CORE_EX1_110, new Modifier(0));
            }//如果对面场上有凯恩·血蹄 Cairne Bloodhoof     CORE_EX1_110 ，降低攻击优先度   


            //对面如果是盗贼 巴罗夫拉出来的怪威胁值优先（主要防止战吼怪被回手重新使用）
            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.BAR_072))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.BAR_072, new Modifier(0));
            }//如果对面场上有火刃侍僧 Burning Blade Acolyte     BAR_072 ，降低攻击优先度   

            if (board.MinionEnemy.Any(minion => minion.Template.Id == Card.Cards.SCH_351))
            {
                p.OnBoardBoardEnemyMinionsModifiers.AddOrUpdate(Card.Cards.SCH_351, new Modifier(200));
            }//如果对面场上有詹迪斯·巴罗夫 Jandice Barov     SCH_351 ，提高攻击优先度  


            #endregion

//德：DRUID 猎：HUNTER 法：MAGE 骑：PALADIN 牧：PRIEST 贼：ROGUE 萨：SHAMAN 术：WARLOCK 战：WARRIOR 瞎：DEMONHUNTER
            return p;
        }}
        
        //芬利·莫格顿爵士技能选择
        public Card.Cards SirFinleyChoice(List<Card.Cards> choices)
        {
            var filteredTable = _heroPowersPriorityTable.Where(x => choices.Contains(x.Key)).ToList();
            return filteredTable.First(x => x.Value == filteredTable.Max(y => y.Value)).Key;
        }

        //卡扎库斯选择
        public Card.Cards KazakusChoice(List<Card.Cards> choices)
        {
            return choices[0];
        }

        //计算类
        public static class BoardHelper
        {
            //得到敌方的血量和护甲之和
            public static int GetEnemyHealthAndArmor(Board board)
            {
                return board.HeroEnemy.CurrentHealth + board.HeroEnemy.CurrentArmor;
            }

            //得到自己的法强
            public static int GetSpellPower(Board board)
            {
                //计算没有被沉默的随从的法术强度之和
                return board.MinionFriend.FindAll(x => x.IsSilenced == false).Sum(x => x.SpellPower);
            }

            //获得第二轮斩杀血线
            public static int GetSecondTurnLethalRange(Board board)
            {
                //敌方英雄的生命值和护甲之和减去可释放法术的伤害总和
                return GetEnemyHealthAndArmor(board) - GetPlayableSpellSequenceDamages(board);
            }

            //下一轮是否可以斩杀敌方英雄
            public static bool HasPotentialLethalNextTurn(Board board)
            {
                //如果敌方随从没有嘲讽并且造成伤害
                //(敌方生命值和护甲的总和 减去 下回合能生存下来的当前场上随从的总伤害 减去 下回合能攻击的可使用随从伤害总和)
                //后的血量小于总法术伤害
                if (!board.MinionEnemy.Any(x => x.IsTaunt) &&
                    (GetEnemyHealthAndArmor(board) - GetPotentialMinionDamages(board) - GetPlayableMinionSequenceDamages(GetPlayableMinionSequence(board), board))
                        <= GetTotalBlastDamagesInHand(board))
                {
                    return true;
                }
                //法术释放过敌方英雄的血量是否大于等于第二轮斩杀血线
                return GetRemainingBlastDamagesAfterSequence(board) >= GetSecondTurnLethalRange(board);
            }

            //获得下回合能生存下来的当前场上随从的总伤害
            public static int GetPotentialMinionDamages(Board board)
            {
                return GetPotentialMinionAttacker(board).Sum(x => x.CurrentAtk);
            }

            //获得下回合能生存下来的当前场上随从集合
            public static List<Card> GetPotentialMinionAttacker(Board board)
            {
                //下回合能生存下来的当前场上随从集合
                var minionscopy = board.MinionFriend.ToArray().ToList();

                //遍历 以敌方随从攻击力 降序排序 的 场上敌方随从集合
                foreach (var mi in board.MinionEnemy.OrderByDescending(x => x.CurrentAtk))
                {
                    //以友方随从攻击力 降序排序 的 场上的所有友方随从集合，如果该集合存在生命值大于与敌方随从攻击力
                    if (board.MinionFriend.OrderByDescending(x => x.CurrentAtk).Any(x => x.CurrentHealth <= mi.CurrentAtk))
                    {
                        //以友方随从攻击力 降序排序 的 场上的所有友方随从集合,找出该集合中友方随从的生命值小于等于敌方随从的攻击力的随从
                        var tar = board.MinionFriend.OrderByDescending(x => x.CurrentAtk).FirstOrDefault(x => x.CurrentHealth <= mi.CurrentAtk);
                        //将该随从移除掉
                        minionscopy.Remove(tar);
                    }
                }

                return minionscopy;
            }

            //获得下回合能生存下来的对面随从集合
            public static List<Card> GetSurvivalMinionEnemy(Board board)
            {
                //下回合能生存下来的当前对面场上随从集合
                var minionscopy = board.MinionEnemy.ToArray().ToList();

                //遍历 以友方随从攻击力 降序排序 的 场上友方可攻击随从集合
                foreach (var mi in board.MinionFriend.FindAll(x => x.CanAttack).OrderByDescending(x => x.CurrentAtk))
                {
                    //如果存在友方随从攻击力大于等于敌方随从血量
                    if (board.MinionEnemy.OrderByDescending(x => x.CurrentHealth).Any(x => x.CurrentHealth <= mi.CurrentAtk))
                    {
                        //以敌方随从血量降序排序的所有敌方随从集合，找出敌方生命值小于等于友方随从攻击力的随从
                        var tar = board.MinionEnemy.OrderByDescending(x => x.CurrentHealth).FirstOrDefault(x => x.CurrentHealth <= mi.CurrentAtk);
                        //将该随从移除掉
                        minionscopy.Remove(tar);
                    }
                }
                return minionscopy;
            }

            //获取可以使用的随从集合
            public static List<Card.Cards> GetPlayableMinionSequence(Board board)
            {
                //卡片集合
                var ret = new List<Card.Cards>();

                //当前剩余的法力水晶
                var manaAvailable = board.ManaAvailable;

                //遍历以手牌中费用降序排序的集合
                foreach (var card in board.Hand.OrderByDescending(x => x.CurrentCost))
                {
                    //如果当前卡牌不为随从，继续执行
                    if (card.Type != Card.CType.MINION) continue;

                    //当前法力值小于卡牌的费用，继续执行
                    if (manaAvailable < card.CurrentCost) continue;

                    //添加到容器里
                    ret.Add(card.Template.Id);

                    //修改当前使用随从后的法力水晶
                    manaAvailable -= card.CurrentCost;
                }

                return ret;
            }

            //获取可以使用的奥秘集合
            public static List<Card.Cards> GetPlayableSecret(Board board)
            {
                //卡片集合
                var ret = new List<Card.Cards>();

                //遍历手牌中所有奥秘集合
                foreach (var card1 in board.Hand.FindAll(card => card.Template.IsSecret))
                {
                    if (board.Secret.Count > 0)
                    {
                        //遍历头上奥秘集合
                        foreach (var card2 in board.Secret.FindAll(card => CardTemplate.LoadFromId(card).IsSecret))
                        {

                            //如果手里奥秘和头上奥秘不相等
                            if (card1.Template.Id != card2)
                            {
                                //添加到容器里
                                ret.Add(card1.Template.Id);
                            }
                        }
                    }
                    else
                    { ret.Add(card1.Template.Id); }
                }
                return ret;
            }


            //获取下回合能攻击的可使用随从伤害总和
            public static int GetPlayableMinionSequenceDamages(List<Card.Cards> minions, Board board)
            {
                //下回合能攻击的可使用随从集合攻击力相加
                return GetPlayableMinionSequenceAttacker(minions, board).Sum(x => CardTemplate.LoadFromId(x).Atk);
            }

            //获取下回合能攻击的可使用随从集合
            public static List<Card.Cards> GetPlayableMinionSequenceAttacker(List<Card.Cards> minions, Board board)
            {
                //未处理的下回合能攻击的可使用随从集合
                var minionscopy = minions.ToArray().ToList();

                //遍历 以敌方随从攻击力 降序排序 的 场上敌方随从集合
                foreach (var mi in board.MinionEnemy.OrderByDescending(x => x.CurrentAtk))
                {
                    //以友方随从攻击力 降序排序 的 场上的所有友方随���集合，如果该集合存在生命值大于与敌方随从攻击力
                    if (minions.OrderByDescending(x => CardTemplate.LoadFromId(x).Atk).Any(x => CardTemplate.LoadFromId(x).Health <= mi.CurrentAtk))
                    {
                        //以友方随从攻击力 降序排序 的 场上的所有友方随从集合,找出该集合中友方随从的生命值小于等于敌方随从的攻击力的随从
                        var tar = minions.OrderByDescending(x => CardTemplate.LoadFromId(x).Atk).FirstOrDefault(x => CardTemplate.LoadFromId(x).Health <= mi.CurrentAtk);
                        //将该随从移除掉
                        minionscopy.Remove(tar);
                    }
                }

                return minionscopy;
            }

            //获取当前回合手牌中的总法术伤害
            public static int GetTotalBlastDamagesInHand(Board board)
            {
                //从手牌中找出法术伤害表存在的法术的伤害总和(包括法强)
                return
                    board.Hand.FindAll(x => _spellDamagesTable.ContainsKey(x.Template.Id))
                        .Sum(x => _spellDamagesTable[x.Template.Id] + GetSpellPower(board));
            }

            //获取可以使用的法术集合
            public static List<Card.Cards> GetPlayableSpellSequence(Board board)
            {
                //卡片集合
                var ret = new List<Card.Cards>();

                //当前剩余的法力水晶
                var manaAvailable = board.ManaAvailable;

                if (board.Secret.Count > 0)
                {
                    //遍历以手牌中费用降序排序的集合
                    foreach (var card in board.Hand.OrderBy(x => x.CurrentCost))
                    {
                        //如果手牌中又不在法术序列的法术牌，继续执行
                        if (_spellDamagesTable.ContainsKey(card.Template.Id) == false) continue;

                        //当前法力值小于卡牌的费用，继续执行
                        if (manaAvailable < card.CurrentCost) continue;

                        //添加到容器里
                        ret.Add(card.Template.Id);

                        //修改当前使用随从后的法力水晶
                        manaAvailable -= card.CurrentCost;
                    }
                }
                else if (board.Secret.Count == 0)
                {
                    //遍历以手牌中费用降序排序的集合
                    foreach (var card in board.Hand.FindAll(x => x.Type == Card.CType.SPELL).OrderBy(x => x.CurrentCost))
                    {
                        //如果手牌中又不在法术序列的法术牌，继续执行
                        if (_spellDamagesTable.ContainsKey(card.Template.Id) == false) continue;

                        //当前法力值小于卡牌的费用，继续执行
                        if (manaAvailable < card.CurrentCost) continue;

                        //添加到容器里
                        ret.Add(card.Template.Id);

                        //修改当前使用随从后的法力水晶
                        manaAvailable -= card.CurrentCost;
                    }
                }

                return ret;
            }
            
            //获取存在于法术列表中的法术集合的伤害总和(包括法强)
            public static int GetSpellSequenceDamages(List<Card.Cards> sequence, Board board)
            {
                return
                    sequence.FindAll(x => _spellDamagesTable.ContainsKey(x))
                        .Sum(x => _spellDamagesTable[x] + GetSpellPower(board));
            }

            //得到可释放法术的伤害总和
            public static int GetPlayableSpellSequenceDamages(Board board)
            {
                return GetSpellSequenceDamages(GetPlayableSpellSequence(board), board);
            }
            
            //计算在法术释放过敌方英雄的血量
            public static int GetRemainingBlastDamagesAfterSequence(Board board)
            {
                //当前回合总法术伤害减去可释放法术的伤害总和
                return GetTotalBlastDamagesInHand(board) - GetPlayableSpellSequenceDamages(board);
            }

            public static bool IsOutCastCard(Card card, Board board)
            {
                var OutcastLeft = board.Hand.Find(x => x.CurrentCost >= 0);
                var OutcastRight = board.Hand.FindLast(x => x.CurrentCost >= 0);
                if (card.Template.Id == OutcastLeft.Template.Id
                    || card.Template.Id == OutcastRight.Template.Id)
                {
                    return true;
                    
                }
                return false;
            }
            public static bool IsGuldanOutCastCard(Card card, Board board)
            {
                if ((board.FriendGraveyard.Exists(x => CardTemplate.LoadFromId(x).Id == Card.Cards.BT_601)
                    && card.Template.Cost - card.CurrentCost == 3))
                {
                    return true;
                }
                
                return false;
            }
            public static bool  IsOutcast(Card card,Board board)
            {
                if(IsOutCastCard(card,board) || IsGuldanOutCastCard(card,board))
                {
                    return true;
                }
                return false;
            }


            //在没有法术的情况下有潜在致命的下一轮
            public static bool HasPotentialLethalNextTurnWithoutSpells(Board board)
            {
                if (!board.MinionEnemy.Any(x => x.IsTaunt) &&
                    (GetEnemyHealthAndArmor(board) -
                     GetPotentialMinionDamages(board) -
                     GetPlayableMinionSequenceDamages(GetPlayableMinionSequence(board), board) <=
                     0))
                {
                    return true;
                }
                return false;
            }
        }
    }
}