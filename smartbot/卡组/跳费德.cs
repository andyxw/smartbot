### 跳费德
# 职业：德鲁伊
# 模式：标准模式
# 狮鹫年
#
# 2x (0) 激活
# 2x (0) 水栖形态 TSC_654
# 1x (1) 森林之王伊弗斯
# 2x (1) 暗礁德鲁伊 DED_001
# 2x (1) 安插证据 REV_313 
# 2x (2) 月光指引 DED_002 
# 2x (2) 应急木工 DED_003 
# 2x (2) 大地之鳞
# 1x (3) 雷纳索尔王子
# 2x (3) 野性成长 CORE_CS2_013
# 1x (3) 艾露恩神谕者
# 1x (3) 掩息海星
# 1x (3) 开路者
# 1x (3) 布莱恩·铜须
# 1x (4) 癫狂公爵西塔尔
# 2x (4) 孀花播种者 REV_318
# 1x (5) 野性之心古夫 AV_205
# 1x (5) 话痨奥术师
# 2x (5) 滋养
# 1x (5) 划水好友
# 1x (7) 灌木巨龙托匹奥
# 2x (7) 奥妮克希亚鳞片
# 2x (8) 奇迹生长
# 1x (9) 贪食的吞噬者
# 1x (10) 德纳修斯大帝 REV_906
# 1x (10) 团本首领奥妮克希亚
# 2x (20) 纳迦巨人
#
AAECAZICDsn1A4mLBIeNBKWNBPGkBKWtBI21BPa9BOnQBJjUBLjZBO/eBODtBJfvBA2sgASvgASwgASJnwSunwTanwTPrASNsgT/vQSuwATW3gTB3wTaoQUA
# 想要使用这副套牌，请先复制到剪贴板，再在游戏中创建新套牌。
# 套牌详情请查看https://hsreplay.net/decks/5adyd4SX9GBlBPRuc1sWyc/
### 自定义 德鲁伊
# 职业：德鲁伊
# 模式：标准模式
# 多头蛇年
#
# 2x (0) 水栖形态
# 2x (0) 激活
# 2x (1) 嗜睡的藻农 TSC_657
# 2x (1) 安插证据
# 2x (1) 暗礁德鲁伊
# 1x (1) 森林之王伊弗斯 AV_142t 
# 2x (2) 大地之鳞
# 2x (2) 应急木工
# 2x (2) 月光指引
# 1x (3) 布莱恩·铜须
# 1x (3) 掩息海星
# 1x (3) 艾露恩神谕者 SW_419 
# 2x (3) 野性成长
# 1x (3) 雷纳索尔王子
# 2x (4) 孀花播种者
# 1x (4) 癫狂公爵西塔尔
# 2x (5) 滋养
# 1x (5) 野性之心古夫
# 2x (7) 奥妮克希亚鳞片
# 1x (7) 灌木巨龙托匹奥
# 2x (8) 奇迹生长
# 2x (9) 贪食的吞噬者
# 1x (10) 团本首领奥妮克希亚
# 1x (10) 德纳修斯大帝
# 2x (20) 纳迦巨人 TSC_829
# 
AAECAc6LBQrJ9QOJiwTxpASlrQSNtQTp0ASY1AS42QTv3gSX7wQPrIAEr4AEsIAEiZ8Erp8E2p8Ez6wEjbIE/70E8L8ErsAE1t4Ewd8E4O0E2qEFAA==
# 
# 想要使用这副套牌，请先复制到剪贴板，然后在游戏中点击“新套牌”进行粘贴。