### 圣物瞎
# 职业：恶魔猎手
# 模式：标准模式
# 狮鹫年
#
# 2x (1) 獠牙锥刃 BAR_330
# 2x (1) 灭绝圣物 REV_834
# 2x (1) 敏捷咒符 SW_041
# 2x (2) 圣物仓库 REV_942
# 2x (2) 剃刀野猪 BAR_325
# 2x (3) 邪能响尾蛇
# 2x (3) 幻灭心能圣物 REV_943
# 1x (3) 布莱恩·铜须
# 2x (3) 宝藏守卫 TSC_938
# 2x (3) 剃刀沼泽兽王 BAR_326
# 1x (4) 贪食的克里克西斯
# 1x (4) 侧翼合击
# 1x (5) 泰兰·弗丁
# 2x (5) 次元圣物 Relic of Dimensions ID：REV_508 
# 1x (6) 逐罪者凯尔萨斯
# 1x (6) 裂魔者库尔特鲁斯
# 1x (7) 渊狱魔犬希拉格
# 1x (7) 亡语者布莱克松
# 1x (8) 圣物匠赛·墨克斯
# 1x (10) 德纳修斯大帝 REV_906
#
AAECAea5Awq/7QOoigSHiwT4lAT+vwTp0ASY1ASq3QSt4gS+8AQKu+0DvO0D/e0Dr+8DivcDh7cEr94EsN4EquIEheUEAA==
# 想要使用这副套牌，请先复制到剪贴板，再在游戏中创建新套牌。
# 套牌详情请查看https://hsreplay.net/decks/Y49zNxoUS3HwumkZk89vgf/