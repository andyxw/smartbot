### 自定义 战士
# 职业：战士
# 模式：标准模式
# 多头蛇年
#
# 2x (1) 战歌大使 BAR_843 
# 2x (1) 赤红深渊 REV_990
# 2x (1) 铁肩冲撞 
# 2x (2) 严酷的监工
# 2x (2) 动乱 REV_337 
# 2x (2) 受伤的托维尔人 CORE_ULD_271
# 2x (2) 心能提取者 REV_332
# 2x (2) 疯狂的可怜鬼 REV_930 
# 2x (2) 蛛魔之卵 FP1_007
# 1x (3) 洛卡拉 BAR_847
# 2x (3) 灌能战斧 REV_933
# 2x (3) 苦痛侍僧 CORE_EX1_007
# 1x (4) 剑圣奥卡尼
# 2x (4) 骄傲罪责
# 1x (5) 贫瘠之地铁匠
# 1x (6) 屠戮者奥格拉 REV_934
# 1x (7) 活体利刃蕾茉妮雅
# 1x (8) 格罗玛什·地狱咆哮
# 
AAECAb7yBAb26AOV7QOLoATHsgSB3ASI3wQMje0DjKAE0aAE06wEnNQEvNsE/9sEvuIEjuMEtuMEiYMFmKQFAA==
# 
# 想要使用这副套牌，请先复制到剪贴板，然后在游戏中点击“新套牌”进行粘贴。
### 激怒战
# 职业：战士
# 模式：标准模式
# 狮鹫年
#
# 2x (1) 赤红深渊
# 2x (1) 泥仆员工 REV_338
# 2x (1) 战歌大使
# 2x (2) 黑曜石铸匠 TSC_942 
# 2x (2) 疯狂的可怜鬼
# 2x (2) 心能提取者
# 2x (2) 受伤的托维尔人
# 2x (2) 严酷的监工
# 2x (3) 雏龙狂魔
# 2x (3) 苦痛侍僧
# 2x (3) 灌能战斧
# 1x (3) 洛卡拉
# 1x (3) 暴乱狂战士
# 2x (4) 旋风争斗者
# 1x (4) 剑圣萨穆罗
# 1x (6) 屠戮者奥格拉
# 1x (7) 重拳先生
# 1x (8) 格罗玛什·地狱咆哮
#
AAECAQcG++gDle0Dv4AEi6AEjaAEgdwEDI3tA5btA4ygBLysBJC3BJzUBLzbBP/bBL7iBKXkBImDBZikBQA=
# 想要使用这副套牌，请先复制到剪贴板，再在游戏中创建新套牌。
# 套牌详情请查看https://hsreplay.net/decks/FBv7sS4iuJFwY4BEnuubef/
### 激怒战
# 职业：战士
# 模式：标准模式
# 狮鹫年
#
# 2x (1) 赤红深渊
# 2x (1) 泥仆员工
# 2x (1) 战歌大使
# 2x (2) 黑曜石铸匠
# 1x (2) 邪恶的厨师
# 2x (2) 疯狂的可怜鬼
# 2x (2) 心能提取者
# 2x (2) 严酷的监工 CORE_EX1_603
# 2x (3) 苦痛侍僧
# 2x (3) 灌能战斧
# 1x (3) 洛卡拉
# 2x (3) 暴乱狂战士 CORE_EX1_604
# 2x (4) 旋风争斗者
# 2x (5) 石槌掌锚手
# 1x (6) 屠戮者奥格拉
# 1x (7) 萨鲁法尔大王
# 1x (7) 活体利刃蕾茉妮雅
# 1x (8) 格罗玛什·地狱咆哮
#
AAECAQcGle0Dqu4Di6AEgdwEiN8E3e0EDI3tA5btA9XxA4ygBI2gBJC3BJzUBLzbBP/bBL7iBKXkBImDBQA=
# 想要使用这副套牌，请先复制到剪贴板，再在游戏中创建新套牌。
# 套牌详情请查看https://hsreplay.net/decks/IogXKIsNd5ydFewOpYLKvg/
