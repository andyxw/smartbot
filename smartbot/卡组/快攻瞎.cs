### 自定义 恶魔猎手
# 职业：恶魔猎手
# 模式：标准模式
# 多头蛇年
#
# 2x (0) 处理证据 REV_507 
# 1x (1) 农夫 SW_319
# 2x (1) 凶恶的滑矛纳迦 TSC_827
# 2x (1) 怒火（等级1）
# 1x (1) 恐惧牢笼战刃
# 1x (1) 战斗邪犬
# 1x (1) 欢快的同伴
# 2x (1) 深铁穴居人
# 2x (2) 历战先锋
# 2x (2) 案卷书虫 Bibliomite ID：REV_511 
# 2x (2) 游荡贤者
# 2x (2) 邪能弹幕
# 2x (3) 捕掠
# 2x (3) 放大战刃
# 1x (3) 赛丝诺女士
# 1x (4) 公诉人梅尔特拉尼克斯
# 2x (4) 盲眼法官 Sightless Magistrate ID：MAW_008 
# 1x (4) 贪食的克里克西斯
# 1x (6) 裂魔者库尔特鲁斯
# 
AAECAdKLBQj09gOHiwSEjQTSnwT7vwSe1ASt4gTu7QQLwvEDifcDyZ8E4aQEiLIEmLoE+b8EpeIEq+IExuIEve0EAA==
# 
# 想要使用这副套牌，请先复制到剪贴板，然后在游戏中点击“新套牌”进行粘贴。